/*****< ss1vserm.h >***********************************************************/
/* Copyright (c) 2015, The Linux Foundation. All rights reserved.             */
/*                                                                            */
/* Permission to use, copy, modify, and/or distribute this software for       */
/* any purpose with or without fee is hereby granted, provided that           */
/* the above copyright notice and this permission notice appear in all        */
/* copies.                                                                    */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL              */
/* WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED              */
/* WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE           */
/* AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL       */
/* DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA         */
/* OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER          */
/* TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR           */
/* PERFORMANCE OF THIS SOFTWARE.                                              */
/******************************************************************************/
/*                                                                            */
/*  SS1VSERM - Stonestreet One Virtual Serial Module Header File for Linux.   */
/*                                                                            */
/******************************************************************************/
#ifndef __SS1VSERMH__
#define __SS1VSERMH__

#include <linux/ioctl.h>                 /* Included for IOCTL MACRO's.       */
#include <linux/serial_reg.h>            /* Included for Serial Constants.    */

#define SS1SER_DEVICE_NAME                    "SS1SER"  /* Defines the Device */
                                                        /* Name of the SS1    */
                                                        /* Virtual Serial Port*/
                                                        /* Driver (Physical   */
                                                        /* Device Side).      */

#define SS1VSER_DEVICE_NAME                  "SS1VSER"  /* Defines the Device */
                                                        /* Name of the SS1    */
                                                        /* Virtual Serial Port*/
                                                        /* Driver (Transport  */
                                                        /* Device Side).      */

#define VSER_DEFAULT_NUMBER_DEVICES                (1)  /* Defines the default*/
                                                        /* number of devices  */
                                                        /* that will be       */
                                                        /* inserted into the  */
                                                        /* system if a number */
                                                        /* is not specified   */
                                                        /* at load time.      */

#define VSER_MAXIMUM_NUMBER_DEVICES               (32)  /* Defines the Maximum*/
                                                        /* number of devices  */
                                                        /* that may be        */
                                                        /* inserted into the  */
                                                        /* system.            */

#define VSER_MINIMUM_NUMBER_DEVICES                (1)  /* Defines the Minimum*/
                                                        /* number of devices  */
                                                        /* that maybe inserted*/
                                                        /* into the system.   */

#define VSER_IOCTL_MAGIC                         ('g')  /* Denotes the magic  */
                                                        /* number associated  */
                                                        /* with this device.  */

#define VSER_IOCTL_SEND_BREAK               _IO(VSER_IOCTL_MAGIC, 0)
                                                        /* Denotes the IOCTL  */
                                                        /* used to emulate    */
                                                        /* that a break has   */
                                                        /* occurred to the    */
                                                        /* device side of the */
                                                        /* Virtual Serial     */
                                                        /* Driver.            */

#define VSER_IOCTL_GET_BREAK_STATUS         _IO(VSER_IOCTL_MAGIC, 1)
                                                        /* Denotes the IOCTL  */
                                                        /* used to get the    */
                                                        /* status of the break*/
                                                        /* event that has     */
                                                        /* occurred.  Note    */
                                                        /* that this IOCTL    */
                                                        /* must be called     */
                                                        /* after receiving a  */
                                                        /* Break Event via the*/
                                                        /* WAIT_EVENT IOCTL   */
                                                        /* reset the event.   */

#define VSER_IOCTL_GET_MODEM_STATUS         _IOR(VSER_IOCTL_MAGIC, 2, unsigned int *)
                                                        /* Denotes the IOCTL  */
                                                        /* used to retrieve   */
                                                        /* the current value  */
                                                        /* of the modem status*/
                                                        /* register. This     */
                                                        /* IOCTL accepts a    */
                                                        /* pointer to an      */
                                                        /* unsigned int that  */
                                                        /* the modem status   */
                                                        /* mask will be       */
                                                        /* returned in.       */

#define VSER_IOCTL_SET_MODEM_STATUS         _IOW(VSER_IOCTL_MAGIC, 3, unsigned int)
                                                        /* Denotes the IOCTL  */
                                                        /* used to set the    */
                                                        /* current value of   */
                                                        /* the modem status   */
                                                        /* register. This     */
                                                        /* IOCTL accepts an   */
                                                        /* unsigned int as its*/
                                                        /* parameter to set   */
                                                        /* the modem status   */
                                                        /* register.          */

#define VSER_IOCTL_GET_LINE_STATUS          _IOR(VSER_IOCTL_MAGIC, 4, unsigned int *)
                                                        /* Denotes the IOCTL  */
                                                        /* used to retrieve   */
                                                        /* the current value  */
                                                        /* of the line status */
                                                        /* register. This     */
                                                        /* IOCTL accepts a    */
                                                        /* pointer to an      */
                                                        /* unsigned int that  */
                                                        /* the line status    */
                                                        /* mask will be       */
                                                        /* returned in.       */

#define VSER_IOCTL_SET_LINE_STATUS          _IOW(VSER_IOCTL_MAGIC, 5, unsigned int)
                                                        /* Denotes the IOCTL  */
                                                        /* used to set the    */
                                                        /* current value of   */
                                                        /* the line status    */
                                                        /* register.  This    */
                                                        /* IOCTL accepts an   */
                                                        /* unsigned int as its*/
                                                        /* parameter to set   */
                                                        /* the line status    */
                                                        /* register.          */

#define VSER_IOCTL_WAIT_SERIAL_EVENT        _IOR(VSER_IOCTL_MAGIC, 6, unsigned long *)
                                                        /* Denotes the IOCTL  */
                                                        /* that can be used   */
                                                        /* to wait on         */
                                                        /* notification for a */
                                                        /* serial event to    */
                                                        /* occur.  This IOCTL */
                                                        /* accepts a pointer  */
                                                        /* to a Wait Event    */
                                                        /* mask as its        */
                                                        /* parameter and      */
                                                        /* returns a mask of  */
                                                        /* the Events that    */
                                                        /* have occurred since*/
                                                        /* the function was   */
                                                        /* called in the      */
                                                        /* argument pointer   */
                                                        /* that was passed in.*/

#define VSER_IOCTL_RECEIVE_DATA_LENGTH      _IOR(VSER_IOCTL_MAGIC, 7, unsigned int *)
                                                        /* Denotes the IOCTL  */
                                                        /* that can be used to*/
                                                        /* read the receive   */
                                                        /* buffer data length.*/
                                                        /* This IOCTL accepts */
                                                        /* a pointer to an    */
                                                        /* unsigned int that  */
                                                        /* the read buffer    */
                                                        /* data length will   */
                                                        /* be returned in.    */

   /* The following Constants represent the Bit Mask that specifies the */
   /* Event Mask for the VSER_IOCTL_WAIT_SERIAL_EVENT to wait on.  These*/
   /* Constants also represent the event that occurred causing the wait */
   /* event to return.                                                  */
#define VSER_BREAK_EVENT                      (0x00000001)
#define VSER_LINE_STATUS_EVENT                (0x00000002)
#define VSER_MODEM_STATUS_EVENT               (0x00000004)
#define VSER_RECEIVE_DATA_EVENT               (0x00000008)

   /* The following Constants represent the Bit Mask that specifies the */
   /* value of an individual Modem/Port Control Signal.  If the         */
   /* specified Bit has a binary value of '1', then the Signal is       */
   /* considered to be set, else it is considered NOT set (clear).  This*/
   /* Bit Mask is used with the Modem Status IOCTL's.                   */
#define VSER_MODEM_STATUS_DTR                  (TIOCM_DTR)
#define VSER_MODEM_STATUS_RTS                  (TIOCM_RTS)
#define VSER_MODEM_STATUS_CTS                  (TIOCM_CTS)
#define VSER_MODEM_STATUS_CD                    (TIOCM_CD)
#define VSER_MODEM_STATUS_RNG                  (TIOCM_RNG)
#define VSER_MODEM_STATUS_DSR                  (TIOCM_DSR)

   /* The following Constants represent the Bit Mask that specifies the */
   /* value of an individual Line Status Signal.  If the specified Bit  */
   /* has a binary value of '1', then the Signal is considered to be    */
   /* set, else it is considered NOT set (clear).  This Bit Mask is used*/
   /* with the Line Status IOCTL's.                                     */
#define VSER_LINE_STATUS_BREAK_INDICATION    (UART_LSR_BI)
#define VSER_LINE_STATUS_FRAMING_ERROR       (UART_LSR_FE)
#define VSER_LINE_STATUS_PARITY_ERROR        (UART_LSR_PE)
#define VSER_LINE_STATUS_OVERRUN_ERROR       (UART_LSR_OE)

#endif
