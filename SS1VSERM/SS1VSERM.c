/*****< ss1vserm.c >***********************************************************/
/* Copyright (c) 2015, The Linux Foundation. All rights reserved.             */
/*                                                                            */
/* Permission to use, copy, modify, and/or distribute this software for       */
/* any purpose with or without fee is hereby granted, provided that           */
/* the above copyright notice and this permission notice appear in all        */
/* copies.                                                                    */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL              */
/* WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED              */
/* WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE           */
/* AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL       */
/* DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA         */
/* OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER          */
/* TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR           */
/* PERFORMANCE OF THIS SOFTWARE.                                              */
/******************************************************************************/
/*                                                                            */
/*  SS1VSERM - Stonestreet One Virtual Serial Module for Linux.               */
/*                                                                            */
/******************************************************************************/
#include <linux/module.h>       /* Included for module MACRO's.               */
#include <linux/init.h>         /* Included for MACRO's used in marking module*/
                                /* initialization and cleanup functions.      */
#include <linux/kernel.h>       /* Included for printk().                     */
#include <linux/slab.h>         /* Included for kmalloc() and kfree().        */
#include <linux/semaphore.h>    /* Included for kernel semaphore functions.   */
#include <linux/tty.h>          /* Included for TTY Prototypes/Constants.     */
#include <linux/tty_driver.h>   /* Included for TTY struct tty_driver.        */
#include <linux/tty_flip.h>     /* Included for tty_schedule_flip().          */
#include <asm/uaccess.h>        /* Included for copy_to/from_user().          */
#include <asm/string.h>         /* Included for memcpy()/memset().            */
#include <linux/types.h>        /* Included for Linux Type definitions.       */
#include <linux/errno.h>        /* Included for Error Code definitions.       */
#include <linux/serial.h>       /* Included for Serial Interrupt Structure.   */
#include <linux/sched.h>        /* Included for Wait Queue Functions.         */
#include <linux/interrupt.h>    /* Included for mark_bh().                    */
#include <linux/version.h>

#include "SS1VSERM.h"           /* Main Module Prototypes/Constants.          */

//xxx#define DEBUG_ENABLE
#ifdef DEBUG_ENABLE

   #define DEBUGPRINT(format, args...)  printk(format, ##args)

#else

   #define DEBUGPRINT(format, args...)

#endif

   /* Denotes the minimum input buffer size with respect to the device  */
   /* side of this driver.                                              */
#define VSER_DEVICE_INPUT_BUFFER_SIZE                       16384

   /* Work Queue Name.  The work queue allows the driver to wake up a   */
   /* function waiting on the wait queue.  Using a work queue allows    */
   /* this wakeup to happen in an interrupt context.                    */
#define VSER_WORK_QUEUE_NAME                                "VSERWorkQueue"

MODULE_LICENSE("Dual BSD/GPL");

   /* VSER Driver Information Block.  This structure contains ALL       */
   /* information associated with a specific Minor Device Number (member*/
   /* is present in this structure).                                    */
typedef struct _tagDriverInfo_t
{
   struct work_struct             WriteTaskWork;
   struct semaphore               DriverMutex;
   unsigned int                   DeviceOpen;
   unsigned int                   NumberDevicesOpen;
   unsigned int                   TransportOpen;
   unsigned int                   ShutDown;
   unsigned int                   InputBufferLength;
   unsigned int                   InputBufferSize;
   unsigned int                   InputBufferReadPointer;
   unsigned int                   InputBufferWritePointer;
   unsigned char                 *InputBuffer;
   unsigned int                   ReadLength;
   unsigned int                   ModemStatusRegister;
   unsigned int                   LineStatusRegister;
   unsigned long                  WaitEventMask;
   wait_queue_head_t              WaitEventWaitQueue;
   wait_queue_head_t              ModemStatusWaitQueue;
   struct serial_icounter_struct  InterruptCount;
   int                            CdevAdded;
   struct cdev                    DriverCdev;
   struct tty_struct             *TTYStruct;
} DriverInfo_t;

   /* Internal Variables to this Module (Remember that all variables    */
   /* declared static are initialized to 0 automatically by the         */
   /* compiler as part of standard C/C++).                              */
static int NumberOfDevices;                     /* Variable which holds the   */
                                                /* number of devices which    */
                                                /* will be available for use  */
                                                /* within the system. This is */
                                                /* a module parameter that    */
                                                /* maybe set at load time with*/
                                                /* the insmod system call.    */

static DriverInfo_t *DriverInfoList;            /* Variable which holds the   */
                                                /* First Entry (Head of List) */
                                                /* of All currently opened    */
                                                /* VSER Drivers.              */

struct file_operations TTY_FileOps;             /* Variable used to hold file */
                                                /* operations info related to */
                                                /* tty driver.                */

static struct tty_driver *TTYDriver;             /* Variable used to hold the  */
                                                /* tty_driver structure       */
                                                /* information for this       */
                                                /* module.                    */

static struct tty_operations TTYOps;            /* Variable used to hold the  */
                                                /* function operations that   */
                                                /* this module provides for   */
                                                /* the device.                */

static struct tty_struct **VSERTable;           /* Array of pointers to       */
                                                /* tty_structs.  This memory  */
                                                /* is used within the         */
                                                /* tty_driver structure and   */
                                                /* only used by the upper     */
                                                /* TTY/Serial driver.         */

static struct ktermios **VSERTermios;           /* Array of pointers to       */
                                                /* termios structures.  This  */
                                                /* memory is used within the  */
                                                /* the tty_driver structure   */
                                                /* and only used by the upper */
                                                /* TTY/Serial driver.         */

static struct ktermios **VSERTermiosLocked;     /* Array of pointers to       */
                                                /* termios structures.  This  */
                                                /* memory is used within the  */
                                                /* the tty_driver structure   */
                                                /* and only used by the upper */
                                                /* TTY/Serial driver.         */

static struct file_operations VSERFileOperations;  /* Variable which holds the*/
                                                /* File Operation structure   */
                                                /* for the character device   */
                                                /* associated with this       */
                                                /* module.                    */

static int VSERCharacterDeviceMajorNumber;      /* Variable which holds the   */
                                                /* Major Number of the        */
                                                /* character device used as   */
                                                /* the Transport side to the  */
                                                /* Virtual Serial Device.     */

static struct workqueue_struct *pWriteWorkQ;    /* Variable that holds a work */
                                                /* queue used to implement a  */
                                                /* 'write software interrupt'.*/

   /* The module_param MACRO is used to declare the Number Of Devices   */
   /* Variable as a Module Parameter to be used at load time to set the */
   /* number of devices to insert into the system.                      */
module_param(NumberOfDevices, int, S_IRUGO);

static void VSERWriteSoftwareInterrupt(struct work_struct *work);
static int VSERDeviceWrite(DriverInfo_t *DriverInfoPtr, int FromUserSpace, const unsigned char *Buffer, int Length);

static int VSER_Device_Open(struct tty_struct *TTYStruct, struct file *FilePointer);
static void VSER_Device_Close(struct tty_struct *TTYStruct, struct file *FilePointer);
static int VSER_Device_Write(struct tty_struct *TTYStruct, const unsigned char *Buffer, int Length);

#if(LINUX_VERSION_CODE < KERNEL_VERSION(2,6,26))
   static void VSER_Device_Put_Character(struct tty_struct *TTYStruct, unsigned char Character);
#else
   static int VSER_Device_Put_Character(struct tty_struct *TTYStruct, unsigned char Character);
#endif

static void VSER_Device_Flush_Characters(struct tty_struct *TTYStruct);
static int VSER_Device_Write_Room(struct tty_struct *TTYStruct);
static int VSER_Device_Characters_In_Buffer(struct tty_struct *TTYStruct);
static void VSER_Device_Flush_Buffer(struct tty_struct *TTYStruct);
static int VSER_Device_Ioctl(struct tty_struct *TTYStruct, struct file *FilePointer, unsigned int Command, unsigned long Parameter);
static void VSER_Device_Set_Termios(struct tty_struct *TTYStruct, struct ktermios *OldAttributes);
static void VSER_Device_Throttle(struct tty_struct *TTYStruct);
static void VSER_Device_UnThrottle(struct tty_struct *TTYStruct);
static void VSER_Device_Stop(struct tty_struct *TTYStruct);
static void VSER_Device_Start(struct tty_struct *TTYStruct);
static void VSER_Device_HangUp(struct tty_struct *TTYStruct);

#if(LINUX_VERSION_CODE < KERNEL_VERSION(2,6,27))
   static void VSER_Device_Break_Control(struct tty_struct *TTYStruct, int State);
#else
   static int VSER_Device_Break_Control(struct tty_struct *TTYStruct, int State);
#endif

static void VSER_Device_Wait_Until_Sent(struct tty_struct *TTYStruct, int Timeout);
static void VSER_Device_Send_XCharacter(struct tty_struct *TTYStruct, char Character);

static int VSER_Transport_Open(struct inode *INode, struct file *FilePointer);
static int VSER_Transport_Close(struct inode *INode, struct file *FilePointer);
static ssize_t VSER_Transport_Read(struct file *FilePointer, char *Buffer, size_t Length, loff_t *LongOffset);
static ssize_t VSER_Transport_Write(struct file *FilePointer, const char *Buffer, size_t Length, loff_t *LongOffset);
static int VSER_Transport_Ioctl(struct inode *INode, struct file *FilePointer, unsigned int Command, unsigned long Parameter);

   /* The following function is responsible for waking up the Wait Event*/
   /* Wake Queue.  The function acts as a task that is ran at interrupt */
   /* level.  This action is performed here to remove the call from the */
   /* context of the Device Write function call.  Performing this action*/
   /* within the Device Write function call caused system hangs to occur*/
   /* when using PPP with this driver as the transport.  This function  */
   /* accepts as it input parameter a pointer to the Driver Information */
   /* Structure for whom this task is running.                          */
static void VSERWriteSoftwareInterrupt(struct work_struct *work)
{
   DriverInfo_t *DriverInfoPtr;

   /* Set the Driver Info equal to the parameter that was passed in.    */
   DriverInfoPtr = container_of(work, struct _tagDriverInfo_t, WriteTaskWork);

   /* Wake up the transport wait event.                                 */
   if(DriverInfoPtr)
      wake_up_interruptible(&(DriverInfoPtr->WaitEventWaitQueue));
}

   /* The following function is responsible for write data to the       */
   /* internal buffer, and if the data length requested to write is     */
   /* greater than can fit in the buffer this function will grow the    */
   /* buffer.  This function accepts as its first input parameter a     */
   /* pointer to the driver information structure associated with this  */
   /* device.  The second parameter to this function is a flag used to  */
   /* indicate if the buffer which is being passed in is located in user*/
   /* space or kernel space.  The final two parameters to this function */
   /* are the buffer of data which is to be written and the length of   */
   /* the data which lies within the buffer.  This function returns the */
   /* number of bytes which were successfully written upon successful   */
   /* execution or a negative error code as defined in errno.h upon all */
   /* errors.                                                           */
static int VSERDeviceWrite(DriverInfo_t *DriverInfoPtr, int FromUserSpace, const unsigned char *Buffer, int Length)
{
   int            ret_val;
   int            AmountWritten = Length;
   unsigned int   TempBufferSize;
   unsigned char *TempBufferPtr;
   unsigned long  UserCopyLength;

   /* Note AmountWritten is initialized to length for cases where we    */
   /* get to use simple memcpy instead of copy from/to user routines.   */
   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);
   DEBUGPRINT(KERN_ERR "DriverInfoPtr %p FromUserSpace %d: Buffer %p Length %d\n", DriverInfoPtr, FromUserSpace, Buffer, Length);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if((DriverInfoPtr) && (Buffer))
   {
      /* The parameters that were passed in appear to be at least       */
      /* semi-valid, check the length to make sure that there is some   */
      /* data to be written.                                            */
      if(Length)
      {
         /* There is at least some data to be written, initialize the   */
         /* return value to indicate success and the Temp Buffer Size   */
         /* equal the the Current Input Buffer Size.                    */
         ret_val        = 0;
         TempBufferSize = DriverInfoPtr->InputBufferSize;

         /* Now let's check to see if we have enought space to buffer   */
         /* the data (and make sure that we won't overrun the buffer.   */
         while((DriverInfoPtr->InputBufferLength + Length) > TempBufferSize)
         {
            /* The data that was passed in will not currently fit in the*/
            /* amount of space that is currently allocated, adjust the  */
            /* size to attempt to allocate to be twice the size of the  */
            /* buffer that was previously attempted.                    */
            TempBufferSize += TempBufferSize;
         }

         /* Check the Temp Buffer Size to see if a new buffer needs to  */
         /* be allocated.                                               */
         if(TempBufferSize != DriverInfoPtr->InputBufferSize)
         {
            /* Temp Buffer Size differs from the old input buffer size  */
            /* create a new buffer of the new size.  The GFP_ATOMIC flag*/
            /* is used to guarantee that the allocation will not sleep. */
            if((TempBufferPtr = (unsigned char *)kmalloc(TempBufferSize, GFP_ATOMIC)) != NULL)
            {
               /* The Temp Buffer Pointer points to a valid buffer      */
               /* therefore the previous input buffer must have been to */
               /* small.  Now we will check to see if our Circular      */
               /* Buffer can be copied in a single Memory Copy (i.e.  it*/
               /* will NOT wrap).                                       */
               if((DriverInfoPtr->InputBufferReadPointer + DriverInfoPtr->InputBufferLength) > DriverInfoPtr->InputBufferSize)
               {
                  /* Copy the data up to the end of the Buffer.         */
                  memcpy(TempBufferPtr, &(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferReadPointer]), (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferReadPointer));

                  /* Now Copy the data starting at the beginning of the */
                  /* Buffer.                                            */
                  memcpy(&(TempBufferPtr[(DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferReadPointer)]), &(DriverInfoPtr->InputBuffer[0]), (DriverInfoPtr->InputBufferLength - (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferReadPointer)));
               }
               else
               {
                  /* The buffer will not wrap, so simply copy the data  */
                  /* into the Buffer.                                   */
                  memcpy(TempBufferPtr, &(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferReadPointer]), DriverInfoPtr->InputBufferLength);
               }

               /* The buffer has been copied, adjust the Input Buffer   */
               /* Read Pointer, the Input Buffer Write Pointer and the  */
               /* Input Buffer Size.                                    */
               DriverInfoPtr->InputBufferSize         = TempBufferSize;
               DriverInfoPtr->InputBufferReadPointer  = 0;
               DriverInfoPtr->InputBufferWritePointer = DriverInfoPtr->InputBufferLength;

               /* Finally free the previous smaller Input Buffer and set*/
               /* the Temp Buffer Pointer to the Input Buffer Pointer.  */
               kfree(DriverInfoPtr->InputBuffer);

               DriverInfoPtr->InputBuffer = TempBufferPtr;
            }
            else
               ret_val = -ENOMEM;
         }

         /* Now Let's make sure that everything is ok so far before     */
         /* copying the Data into the Input Data Buffer.                */
         if(!ret_val)
         {
            /* We need to see if copying the Data into the Buffer       */
            /* directly will cause the Buffer Pointer to wrap.          */
            if((DriverInfoPtr->InputBufferWritePointer + Length) > DriverInfoPtr->InputBufferSize)
            {
               /* Before copying the data check to see if it came from  */
               /* user space or kernel space so that the appropriate    */
               /* copy function can be used.                            */
               if(FromUserSpace)
               {
                  /* Copy the data to fill up to the end of the Buffer. */
                  /* Note that copy_from_user can fail, or not copy all */
                  /* the data.  The return value is the amount left to  */
                  /* be copied.  Handle this error condition.           */
                  UserCopyLength = copy_from_user(&(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferWritePointer]), Buffer, (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferWritePointer));
                  if(!UserCopyLength)
                  {
                     /* Wrote all this data, now Copy the data starting */
                     /* at the beginning of the Buffer.                 */
                     UserCopyLength = copy_from_user(&(DriverInfoPtr->InputBuffer[0]), &(Buffer[(DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferWritePointer)]), (Length - (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferWritePointer)));

                     /* If there is no data left to copy after this     */
                     /* copy, AmountWritten is the total user length,   */
                     /* otherwise it is the total user length less the  */
                     /* amount left from this last write operation.     */
                     if(UserCopyLength == 0)
                        AmountWritten = Length;
                     else
                        AmountWritten = Length - UserCopyLength;
                  }
                  else
                  {
                     /* AmountWritten is the amount requested in the    */
                     /* first partial write minus the amount left to    */
                     /* write. This could be zero bytes written.        */
                     AmountWritten = (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferWritePointer) - UserCopyLength;
                  }
               }
               else
               {
                  /* Copy the data to fill up to the end of the Buffer. */
                  memcpy(&(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferWritePointer]), Buffer, (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferWritePointer));

                  /* Now Copy the data starting at the beginning of the */
                  /* Buffer.                                            */
                  memcpy(&(DriverInfoPtr->InputBuffer[0]), &(Buffer[(DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferWritePointer)]), (Length - (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferWritePointer)));
               }

               DEBUGPRINT(KERN_ERR "&(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferWritePointer] %p\n", &(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferWritePointer]));

               DEBUGPRINT(KERN_ERR "&(DriverInfoPtr->InputBuffer[0] %p\n", &(DriverInfoPtr->InputBuffer[0]));
            }
            else
            {
               /* Before copying the data check to see if it came from  */
               /* user space or kernel space so that the appropriate    */
               /* copy function can be used.                            */
               if(FromUserSpace)
               {
                  /* The buffer will not wrap, so simply copy the data  */
                  /* into the Buffer. Note that copy_from_user can fail,*/
                  /* or not copy all the data.  The return value is the */
                  /* amount left to be copied.  Handle this error       */
                  /* condition.                                         */
                  UserCopyLength = copy_from_user(&(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferWritePointer]), Buffer, Length);

                  /* AmountWritten is the total user length on succes,  */
                  /* otherwise it is the total user length less the     */
                  /* amount left to write from this write operation.    */
                  if(UserCopyLength == 0)
                     AmountWritten = Length;
                  else
                     AmountWritten = Length - UserCopyLength;
               }
               else
               {
                  /* The buffer will not wrap, so simply copy the data  */
                  /* into the Buffer.                                   */
                  memcpy(&(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferWritePointer]), Buffer, Length);
               }

               DEBUGPRINT(KERN_ERR "&(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferWritePointer] %p\n", &(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferWritePointer]));
            }

            /* Increment the Data Write Pointer and the amount of data  */
            /* put into the Input Buffer.                               */
            DriverInfoPtr->InputBufferLength       += AmountWritten;
            DriverInfoPtr->InputBufferWritePointer += AmountWritten;

            /* Increment the transmit Interrupt Counter by the number of*/
            /* bytes written to the buffer.                             */
            DriverInfoPtr->InterruptCount.tx       += AmountWritten;

            /* Wrap the Input Buffer Write Pointer (if needed).         */
            if(DriverInfoPtr->InputBufferWritePointer >= DriverInfoPtr->InputBufferSize)
               DriverInfoPtr->InputBufferWritePointer -= DriverInfoPtr->InputBufferSize;

            /* Set the return value to the number of bytes written to   */
            /* the input buffer.                                        */
            ret_val                                 = AmountWritten;
         }
      }
      else
         ret_val = 0;
   }
   else
      ret_val = -EINVAL;

   return(ret_val);
}

   /* The following function is responsible for cleaning up the VSER    */
   /* module and all data structures declared within.  This function is */
   /* called upon removal of the module from the kernel via the system  */
   /* call rmmod.  Note that the kernel will not call this function if  */
   /* the Module Use Count is greater then zero.                        */
void __exit CleanupVSERModule(void)
{
   int   i;
   dev_t devno;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);
   DEBUGPRINT(KERN_ERR "TTYDriver %p, DriverInfoList %p, VSERTable %p, VSERTermios %p, VSERTermiosLocked %p\n", TTYDriver, DriverInfoList, VSERTable, VSERTermios, VSERTermiosLocked);

   /* All through, first clean up any allocated resources.              */
   if(TTYDriver)
   {
      /* Remove the character and tty devices from the system.          */
      for(i=0;i < TTYDriver->num;i++)
      {
         if(DriverInfoList[i].CdevAdded)
            cdev_del(&(DriverInfoList[i].DriverCdev));

         tty_unregister_device(TTYDriver, i);
      }

      /* Unregister the driver, and return the resources.               */
      tty_unregister_driver(TTYDriver);

      put_tty_driver(TTYDriver);
   }

   if(VSERCharacterDeviceMajorNumber)
   {
      devno = MKDEV(VSERCharacterDeviceMajorNumber, 0);

      unregister_chrdev_region(devno, NumberOfDevices);
   }

   /* Destroy the created work queue.                                   */
   destroy_workqueue(pWriteWorkQ);

   /* Now free all of the memory that may have been allocated when the  */
   /* module was initialized.                                           */
   if(DriverInfoList)
      kfree(DriverInfoList);

   if(VSERTable)
      kfree(VSERTable);

   if(VSERTermios)
      kfree(VSERTermios);

   if(VSERTermiosLocked)
      kfree(VSERTermiosLocked);

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__ );
}

   /* The following function is responsible for initializing the VSER   */
   /* module and ALL data structures required by the module.  This      */
   /* function is called upon insertion of the module into the kernel   */
   /* via the system call insmod.  This function return zero upon       */
   /* successful execution and a negative value on all errors.          */
   /* Newer Linux kernels have a different mechanism for registering    */
   /* char devices, meaning this module is structurally different than  */
   /* the versions written to a 2.4 system, even though the code is     */
   /* quite similar in places.                                          */
int __init InitializeVSERModule(void)
{
   int           ret_val;
   dev_t         dev = 0;
   unsigned int  i;
   void         *ttyRegReturn;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* First check to see if the Number of Devices Parameter was set to a*/
   /* value other then the default.                                     */
   if(!NumberOfDevices)
   {
      /* The Number of Devices Parameter was not set, set it to the     */
      /* default value.                                                 */
      NumberOfDevices = VSER_DEFAULT_NUMBER_DEVICES;
   }

   /* Now check to make sure that the value that was specified for the  */
   /* Number of Devices appears to be valid.                            */
   if((NumberOfDevices >= VSER_MINIMUM_NUMBER_DEVICES) && (NumberOfDevices <= VSER_MAXIMUM_NUMBER_DEVICES))
   {
      /* The Number of Devices appears to be at least semi-valid, now   */
      /* attempt to allocate the memory that is required for this module*/
      /* to work with the specified Number of Devices.                  */
      DriverInfoList    = (DriverInfo_t *)kmalloc(sizeof(DriverInfo_t)*NumberOfDevices, GFP_KERNEL);
      VSERTable         = (struct tty_struct **)kmalloc(sizeof(struct tty_struct *)*NumberOfDevices, GFP_KERNEL);
      VSERTermios       = (struct ktermios **)kmalloc(sizeof(struct ktermios *)*NumberOfDevices, GFP_KERNEL);
      VSERTermiosLocked = (struct ktermios **)kmalloc(sizeof(struct ktermios *)*NumberOfDevices, GFP_KERNEL);
      TTYDriver         = alloc_tty_driver(NumberOfDevices);

      DEBUGPRINT(KERN_ERR "%s : DriverInfoList %p VSERTable %p VSERTermios %p TTYDriver %p\n", __FUNCTION__, DriverInfoList, VSERTable, VSERTermios, TTYDriver);

      /* Use alloc_chrdev_region to dynamically allocate major device   */
      /* number of physical character device (transport side).  Note    */
      /* minor numbers start at 0.                                      */
      VSERCharacterDeviceMajorNumber = 0;

      ret_val                        = alloc_chrdev_region(&dev, 0, NumberOfDevices, SS1VSER_DEVICE_NAME);

      if(ret_val >=0)
      {
         /* Note the major device number that was allocated. Non-zero   */
         /* will be used in cleanup routine to trigger unregistering the*/
         /* character device.                                           */
         VSERCharacterDeviceMajorNumber = MAJOR(dev);
      }

      DEBUGPRINT(KERN_ERR "%s : VSERCharacterDeviceMajorNumber %d, ret_val %d\n", __FUNCTION__, VSERCharacterDeviceMajorNumber, ret_val);

      /* Now make sure that all the memory that was requested was       */
      /* successfully allocated and a device region was setup.          */
      if((DriverInfoList) && (VSERTable) && (VSERTermios) && (VSERTermiosLocked) && (TTYDriver) && (VSERCharacterDeviceMajorNumber))
      {
         /* Initialize the Pointer arrays required by the TTY Device.   */
         memset(VSERTable, 0, sizeof(struct tty_struct *)*NumberOfDevices);
         memset(VSERTermios, 0, sizeof(struct ktermios *)*NumberOfDevices);
         memset(VSERTermiosLocked, 0, sizeof(struct ktermios *)*NumberOfDevices);

         /* Create a work queue, note same work queue is use for all    */
         /* devices controlled by the driver.                           */
         pWriteWorkQ = create_singlethread_workqueue(VSER_WORK_QUEUE_NAME);

         DEBUGPRINT(KERN_ERR "%s : WorkQueue %s handle %p\n", __FUNCTION__, VSER_WORK_QUEUE_NAME, pWriteWorkQ);

         /* Populate file ops structure (VSERFileOperations) used for   */
         /* cdev (kernel internal character device representation).     */
         VSERFileOperations.owner    = THIS_MODULE;
         VSERFileOperations.open     = VSER_Transport_Open;
         VSERFileOperations.release  = VSER_Transport_Close;
         VSERFileOperations.read     = VSER_Transport_Read;
         VSERFileOperations.write    = VSER_Transport_Write;
         VSERFileOperations.ioctl    = VSER_Transport_Ioctl;

         /* Initialize the Driver Information List.                     */
         ret_val = 0;
         for(i=0;(i < NumberOfDevices) && (!ret_val);i++)
         {
            memset(&DriverInfoList[i], 0, sizeof(DriverInfo_t));

            init_MUTEX(&DriverInfoList[i].DriverMutex);
            init_waitqueue_head(&DriverInfoList[i].WaitEventWaitQueue);
            init_waitqueue_head(&DriverInfoList[i].ModemStatusWaitQueue);

            INIT_WORK(&(DriverInfoList[i].WriteTaskWork), (work_func_t)VSERWriteSoftwareInterrupt);

            /* Initialize the cdev structure and add to kernel.         */
            cdev_init(&(DriverInfoList[i].DriverCdev), &VSERFileOperations);

            DriverInfoList[i].DriverCdev.owner = THIS_MODULE;

            dev                                = MKDEV(VSERCharacterDeviceMajorNumber, i);

            ret_val                            = cdev_add(&(DriverInfoList[i].DriverCdev), dev, 1);

            DEBUGPRINT(KERN_ERR "%s : cdev_add %d returns %d\n", __FUNCTION__, i, ret_val);

            if(ret_val)
               DriverInfoList[i].CdevAdded = 0;
            else
               DriverInfoList[i].CdevAdded = 1;
         }

         if(!ret_val)
         {
            /* If no errors creating the char devices, continue with    */
            /* the TTY Driver.                                          */
            TTYDriver->magic                = TTY_DRIVER_MAGIC;
            TTYDriver->owner                = THIS_MODULE;
            TTYDriver->driver_name          = "SS1 VSER Driver";
            TTYDriver->name                 = SS1SER_DEVICE_NAME;
            TTYDriver->name_base            = 0;

            /* Note that the major number is set to zero here to        */
            /* dynamically allocate the major number.                   */
            TTYDriver->major                = 0;
            TTYDriver->minor_start          = 0;
            TTYDriver->minor_num            = VSER_MAXIMUM_NUMBER_DEVICES;
            TTYDriver->num                  = NumberOfDevices;
            TTYDriver->type                 = TTY_DRIVER_TYPE_SERIAL;
            TTYDriver->subtype              = SERIAL_TYPE_NORMAL;

            /* The device will be initialized to use the standard       */
            /* termios structure, this is defined in tty.h.             */

            TTYDriver->init_termios         = tty_std_termios;
            TTYDriver->init_termios.c_cflag = B9600 | CS8 | CREAD | HUPCL | CLOCAL;

            /* TTY_DRIVER_DYNAMIC_DEV flag tells kernel we register and */
            /* unregister the tty devices.                              */
            TTYDriver->flags                = TTY_DRIVER_REAL_RAW | TTY_DRIVER_DYNAMIC_DEV;

#if(LINUX_VERSION_CODE < KERNEL_VERSION(2,6,28))

            TTYDriver->refcount             = 0;

#endif

            TTYDriver->ttys                 = VSERTable;
            TTYDriver->termios              = VSERTermios;
            TTYDriver->termios_locked       = VSERTermiosLocked;

            TTYOps.open                     = VSER_Device_Open;
            TTYOps.close                    = VSER_Device_Close;
            TTYOps.write                    = VSER_Device_Write;
            TTYOps.put_char                 = VSER_Device_Put_Character;
            TTYOps.flush_chars              = VSER_Device_Flush_Characters;
            TTYOps.write_room               = VSER_Device_Write_Room;
            TTYOps.chars_in_buffer          = VSER_Device_Characters_In_Buffer;
            TTYOps.flush_buffer             = VSER_Device_Flush_Buffer;
            TTYOps.ioctl                    = VSER_Device_Ioctl;
            TTYOps.set_termios              = VSER_Device_Set_Termios;
            TTYOps.throttle                 = VSER_Device_Throttle;
            TTYOps.unthrottle               = VSER_Device_UnThrottle;
            TTYOps.stop                     = VSER_Device_Stop;
            TTYOps.start                    = VSER_Device_Start;
            TTYOps.hangup                   = VSER_Device_HangUp;
            TTYOps.break_ctl                = VSER_Device_Break_Control;
            TTYOps.wait_until_sent          = VSER_Device_Wait_Until_Sent;
            TTYOps.send_xchar               = VSER_Device_Send_XCharacter;

            tty_set_operations(TTYDriver, &TTYOps);

            /* Now that the TTY Driver Structure has been initialized   */
            /* attempt to register the TTY device.                      */
            if((ret_val = tty_register_driver(TTYDriver)) == 0)
            {
               /* The TTY Driver was successfully registered.  Now      */
               /* register the devices controlled.                      */
               for(i = 0; i < NumberOfDevices; i++)
               {

#if(LINUX_VERSION_CODE < KERNEL_VERSION(2,6,18))

                  tty_register_device(TTYDriver, i, NULL);
                  DEBUGPRINT(KERN_ERR "%s : tty_register_device %d\n", __FUNCTION__, i);

#else

                  ttyRegReturn = tty_register_device(TTYDriver, i, NULL);
                  DEBUGPRINT(KERN_ERR "%s : tty_register_device %d returns %p\n", __FUNCTION__, i, ttyRegReturn);

#endif

               }

               /* Signal success.                                       */
               ret_val = 0;
            }
            else
            {
               /* An error occurred while registering the TTY driver.   */
               /* Set return value so cleanup will occur, and return    */
               /* with an error condition.                              */
               DEBUGPRINT(KERN_ERR "%s : register TTYDriver (VSER_Device) failed %d\n", __FUNCTION__, ret_val);

               ret_val = -1;
            }
         }
         else
         {
            /* An error occurred while performing cdev_add. Set return  */
            /* value so cleanup will occur, and return with an error    */
            /* condition.                                               */
            DEBUGPRINT(KERN_ERR "%s : cdev_add failed %d\n", __FUNCTION__, ret_val);

            ret_val = -1;
         }
      }
      else
      {
         DEBUGPRINT(KERN_ERR "%s : memory allocation failure\n", __FUNCTION__);

         /* An error occurred while attempting to allocate memory for   */
         /* the module to use.  Set the return value to indicate an     */
         /* error.                                                      */
         ret_val = -ENOMEM;
      }

      /* Check to see if and error occurred in the initialization of the*/
      /* module.                                                        */
      if(ret_val)
      {
         /* An error occurred in the initialization of the module,      */
         /* let cleanup function clean up everything.                   */
         CleanupVSERModule();
      }
   }
   else
   {
      DEBUGPRINT(KERN_ERR "%s : Number Of Devices is invalid\n", __FUNCTION__);

      /* The Number Of Devices specified is invalid, set the return     */
      /* value to indicate and error.                                   */
      ret_val = -EINVAL;
   }

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__ );

   return(ret_val);
}

   /* The following function is responsible for opening the device side */
   /* of the Virtual Serial Port Driver.  The first parameter to this   */
   /* function is a pointer to the TTY Structure that is associated with*/
   /* this TTY Device.  The second parameter to this function is a      */
   /* pointer to the file structure associated with this device.  This  */
   /* function returns zero upon successful execution or a negative     */
   /* error code as defined in errno.h upon all errors.                 */
static int VSER_Device_Open(struct tty_struct *TTYStruct, struct file *FilePointer)
{
   int           ret_val;
   DriverInfo_t *DriverInfoPtr;

   DEBUGPRINT(KERN_ERR "%s : enter, Minor = %u \n", __FUNCTION__, iminor(FilePointer->f_dentry->d_inode));

   /* Increment the Module Use Count.                                   */
   try_module_get(THIS_MODULE);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if((TTYStruct) && (FilePointer))
   {
      /* The parameters passed in appear to be at least semi-valid.  Now*/
      /* note the driver information structure associated with this     */
      /* Minor Device Number.                                           */
      DriverInfoPtr = &DriverInfoList[iminor(FilePointer->f_dentry->d_inode)];

      /* Next attempt to acquire the semaphore use to protect this      */
      /* Driver Information Entry.                                      */
      if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
      {
         /* Initialize the return value to indicate success.            */
         ret_val = 0;

         /* Check to see if the input buffer has been allocated for this*/
         /* driver entry.                                               */
         if(!DriverInfoPtr->InputBuffer)
         {
            /* The Driver Input Buffer has not been allocated so        */
            /* allocate an input buffer for the driver.                 */
            if((DriverInfoPtr->InputBuffer = (unsigned char *)kmalloc((VSER_DEVICE_INPUT_BUFFER_SIZE*sizeof(unsigned char)), GFP_KERNEL)) != NULL)
            {
               /* The Input Buffer was successfully allocated, now      */
               /* initialize the input buffer parameters.               */
               DriverInfoPtr->InputBufferLength       = 0;
               DriverInfoPtr->InputBufferSize         = VSER_DEVICE_INPUT_BUFFER_SIZE;
               DriverInfoPtr->InputBufferReadPointer  = 0;
               DriverInfoPtr->InputBufferWritePointer = 0;

               /* Reset the Current Read Length.                        */
               DriverInfoPtr->ReadLength              = 0;
            }
            else
               ret_val = -ENOMEM;
         }

         /* Check the return value to see if everything has been        */
         /* successful up to this point.                                */
         if(!ret_val)
         {
            /* Everything is ok so far, now initialize the remaining    */
            /* items pertaining to the Device Side of the Driver in the */
            /* Driver Information Entry.                                */
            DriverInfoPtr->DeviceOpen = 1;
            DriverInfoPtr->ShutDown   = 0;
            DriverInfoPtr->TTYStruct  = TTYStruct;

            /* Check to see if this is the first time the device side   */
            /* has been opened.                                         */
            if(!DriverInfoPtr->NumberDevicesOpen)
            {
               /* This is the first time the device side of the driver  */
               /* has been opened.  Initialize the Interrupt Count      */
               /* Structure.                                            */
               memset(&DriverInfoPtr->InterruptCount, 0, sizeof(struct serial_icounter_struct));

               /* Reset the Modem Status and Line Status registers.     */
               DriverInfoPtr->LineStatusRegister  = 0;
               DriverInfoPtr->ModemStatusRegister = 0;
            }

            /* Increment the number of times the device side has been   */
            /* opened.                                                  */
            DriverInfoPtr->NumberDevicesOpen++;

            /* Finally set the Driver Data in the TTY Structure to point*/
            /* to this entry for use with the rest of the functions     */
            /* associated with this driver.                             */
            TTYStruct->driver_data = (void *)DriverInfoPtr;
         }

         /* Release the semaphore used to protect the Driver Information*/
         /* Entry.                                                      */
         up(&DriverInfoPtr->DriverMutex);
      }
      else
         ret_val = -ENODEV;
   }
   else
      ret_val = -EINVAL;

   /* Check to see if an error occurred, if so decrement the Module Use */
   /* Count.                                                            */
   if(ret_val)
   {
      /* Decrement the use count of the module                          */
      module_put(THIS_MODULE);
   }

   DEBUGPRINT(KERN_ERR "%s : exit, ret_val = %d\n", __FUNCTION__, ret_val);

   return(ret_val);
}

   /* The following function is responsible for closing the device side */
   /* of the Virtual Serial Port Driver.  Note that the memory          */
   /* associated with this device is only freed if both sides of the    */
   /* driver are closed.  The first parameter to this function is a     */
   /* pointer to the TTY Structure that is associated with this TTY     */
   /* Device.  The second parameter to this function is a pointer to the*/
   /* file structure associated with this device.                       */
static void VSER_Device_Close(struct tty_struct *TTYStruct, struct file *FilePointer)
{
   DriverInfo_t *DriverInfoPtr;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if((TTYStruct) && (FilePointer))
   {
      /* The parameters passed in appear to be at least semi-valid.  Now*/
      /* note the driver information structure associated with this     */
      /* Minor Device Number.                                           */
      DriverInfoPtr = &DriverInfoList[iminor(FilePointer->f_dentry->d_inode)];

      /* Next attempt to acquire the semaphore use to protect this      */
      /* Driver Information Entry.                                      */
      if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
      {
         /* The semaphore protecting the entry has been successfully    */
         /* acquired, decrement the Number of Device Sides which remain */
         /* open and check to see if there are anymore open before      */
         /* proceeding.                                                 */
         if(!(--DriverInfoPtr->NumberDevicesOpen))
         {
            /* There are no outstanding open device sides to the driver,*/
            /* if either the Transport Side of the Driver remains open  */
            /* or not change the state of the Device side of the Driver */
            /* and reset the driver data member of the TTY Structure.   */
            DriverInfoPtr->DeviceOpen = 0;
            TTYStruct->driver_data    = NULL;

            /* Next check to see if the transport side of the device    */
            /* remains open.                                            */
            if(!DriverInfoPtr->TransportOpen)
            {
               /* The transport side of the driver is not open so clean */
               /* up the driver.  Set the shut down flag here after     */
               /* releasing the semaphore wake up anyone waiting in the */
               /* wait queue.                                           */
               DriverInfoPtr->ShutDown    = 1;

               /* Next free the memory allocated for the input buffer.  */
               kfree(DriverInfoPtr->InputBuffer);

               DriverInfoPtr->InputBuffer = NULL;

               /* Now reset the TTY Structure pointer to be invalid.    */
               DriverInfoPtr->TTYStruct   = NULL;
            }
         }

         /* Release the semaphore used to protect the Driver Information*/
         /* Entry.                                                      */
         up(&DriverInfoPtr->DriverMutex);

         /* Wake up anyone who may have been waiting now that we don't  */
         /* own any semaphores.                                         */
         wake_up_interruptible(&(DriverInfoPtr->ModemStatusWaitQueue));
         wake_up_interruptible(&(DriverInfoPtr->WaitEventWaitQueue));
      }

      /* Decrement the use count of the module                          */
      module_put(THIS_MODULE);
   }

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

   /* The following function is responsible for writing data to the     */
   /* input buffer associated with this Virtual Serial Port Driver.  The*/
   /* input buffer that this function writes to is the buffer which is  */
   /* read from by the transport sides read function.  The first        */
   /* parameter to this function is a pointer to the TTY Structure that */
   /* is associated with this TTY Device.  The second parameter to this */
   /* function is a flag used to indicate if the buffer which is being  */
   /* passed in is located in user space or kernel space.  The final two*/
   /* parameters to this function are the buffer of data which is to be */
   /* written and the length of the data which lies within the buffer.  */
   /* This function returns the number of bytes which were successfully */
   /* written upon successful execution or a negative error code as     */
   /* defined in errno.h upon all errors.                               */
static int VSER_Device_Write(struct tty_struct *TTYStruct, const unsigned char *Buffer, int Length)
{
   int            ret_val;
   int            FromUserSpace;
   unsigned int   SemaphoreAcquired;
   DriverInfo_t  *DriverInfoPtr;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if(TTYStruct)
   {
      FromUserSpace = in_interrupt();
      DEBUGPRINT(KERN_ERR "%s : FromUserSpace 0x%x\n", __FUNCTION__, FromUserSpace);

      /* The parameters passed in appear to be at least semi-valid.     */
      /* Retrieve the Driver Information for the driver via the driver  */
      /* data member of the TTY Structure.                              */
      DriverInfoPtr = (DriverInfo_t *)TTYStruct->driver_data;

      /* Next check to make sure that the Driver Information Pointer    */
      /* appears to be at least semi-valid.                             */
      if(DriverInfoPtr)
      {
         /* Initialize the semaphore acquired variable to false.        */
         SemaphoreAcquired = 0;

         /* The driver information entry appears to be at least         */
         /* semi-valid, check check to see if this call came from user  */
         /* or kernel space.                                            */
         if(FromUserSpace)
         {
            /* The call originated from user space, get the semaphore to*/
            /* protect the driver information entry.                    */
            if(!down_interruptible(&DriverInfoPtr->DriverMutex))
            {
               /* The semaphore was successfully acquired, set the flag */
               /* to indicate this.                                     */
               SemaphoreAcquired = 1;
            }
         }

         /* Next check to make sure that the semaphore was successfully */
         /* acquired if this call origninated from user space.          */
         if((!FromUserSpace) || ((FromUserSpace) && (SemaphoreAcquired)))
         {
            /* Next check to make sure that the transport side of the   */
            /* device is open.                                          */
            if(DriverInfoPtr->TransportOpen)
            {
               /* The transport side of the driver is open, next attempt*/
               /* to write the data to the device.                      */
               if((ret_val = VSERDeviceWrite(DriverInfoPtr, FromUserSpace, Buffer, Length)) == Length)
               {
                  /* Data was written set the appropriate wait event bit*/
                  /* in the wait event mask and update the amount of    */
                  /* data that is currently being waited on.            */
                  DriverInfoPtr->WaitEventMask |= VSER_RECEIVE_DATA_EVENT;
                  DriverInfoPtr->ReadLength    += (unsigned int)Length;

                  /* Release the semaphore used to protect the Driver   */
                  /* Information Entry if is was previously acquired.   */
                  if(FromUserSpace)
                     up(&DriverInfoPtr->DriverMutex);

                  /* Queue the write software interrupt for immediate   */
                  /* running.                                           */
                  queue_work(pWriteWorkQ, &DriverInfoPtr->WriteTaskWork);
               }
               else
               {
                  /* There was an error while attempting to write the   */
                  /* data to the buffer.  Release the semaphore used to */
                  /* protect the Driver Information Entry if is was     */
                  /* previously acquired.                               */
                  if(FromUserSpace)
                     up(&DriverInfoPtr->DriverMutex);
               }
            }
            else
            {
               /* Release the semaphore used to protect the Driver      */
               /* Information Entry if is was previously acquired.      */
               if(FromUserSpace)
                  up(&DriverInfoPtr->DriverMutex);

               /* The Transport side of the device is not open so return*/
               /* that all of the data was written but do not write any */
               /* data.                                                 */
               ret_val = Length;
            }
         }
         else
         {
            /* An error occurred while attempting to acquire the        */
            /* semaphore, set the return value to indicate that an error*/
            /* occurred.                                                */
            ret_val = -EINTR;
         }
      }
      else
      {
         /* An error has occurred, so set the return value to indicate  */
         /* that an error has occurred.                                 */
         ret_val = -ENODEV;
      }
   }
   else
      ret_val = -EINVAL;

   DEBUGPRINT(KERN_ERR "%s : exit, ret_val = %d\n", __FUNCTION__, ret_val);

   return(ret_val);
}

   /* The following function is responsible for putting a single        */
   /* character into the input buffer associated with this Virtual      */
   /* Serial Port Driver. The input buffer that this function writes to */
   /* is the buffer which is read from by the transport side read       */
   /* function.  The first parameter to this function is a pointer to   */
   /* the TTY Structure that is associated with this TTY Device.  The   */
   /* second parameter to this function is the character to be written  */
   /* to the input buffer.                                              */
#if(LINUX_VERSION_CODE < KERNEL_VERSION(2,6,26))
   static void VSER_Device_Put_Character(struct tty_struct *TTYStruct, unsigned char Character)
#else
   static int VSER_Device_Put_Character(struct tty_struct *TTYStruct, unsigned char Character)
#endif
{
   int           ret_val = 0;
   int           FromUserSpace;
   DriverInfo_t *DriverInfoPtr;

   DEBUGPRINT(KERN_ERR "%s : enter, Character = 0x%02X\n", __FUNCTION__, Character);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if(TTYStruct)
   {
      FromUserSpace = in_interrupt();
      DEBUGPRINT(KERN_ERR "%s : FromUserSpace 0x%x\n", __FUNCTION__, FromUserSpace);

      /* The parameters passed in appear to be at least semi-valid.     */
      /* Retrieve the Driver Information for the driver via the driver  */
      /* data member of the TTY Structure.                              */
      DriverInfoPtr = (DriverInfo_t *)TTYStruct->driver_data;

      /* Next attempt to acquire the semaphore use to protect this      */
      /* Driver Information Entry.                                      */
      if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
      {
         /* The semaphore that protect that actual entry has been       */
         /* successfully acquired, check to see if the Transport side of*/
         /* the driver is open.                                         */
         if(DriverInfoPtr->TransportOpen)
         {
            /* The transport side of the driver is open, next attempt to*/
            /* write the data to the device.                            */
            if(VSERDeviceWrite(DriverInfoPtr, FromUserSpace, &Character, sizeof(unsigned char)) > 0)
            {
               /* Set the data written flag to indicate that some data  */
               /* was place into the input buffer.                      */
               ret_val = 1;

               /* Set the Receive Data Event bit in the Wait Event Mask.*/
               DriverInfoPtr->WaitEventMask |= VSER_RECEIVE_DATA_EVENT;
               DriverInfoPtr->ReadLength    += sizeof(unsigned char);
            }
         }

         /* Release the semaphore used to protect the Driver Information*/
         /* Entry.                                                      */
         up(&DriverInfoPtr->DriverMutex);

         /* Check to see if the data written flag is set indicating that*/
         /* data was put in the buffer.                                 */
         if(ret_val)
         {
            /* Some data was written to the input buffer, set the wait  */
            /* event on the transport side of the driver to indicate    */
            /* data was received.                                       */
            wake_up_interruptible(&(DriverInfoPtr->WaitEventWaitQueue));
         }
      }
   }

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);

#if(LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,26))

   return(ret_val);

#endif
}

static void VSER_Device_Flush_Characters(struct tty_struct *TTYStruct)
{
   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

   /* The following function is responsible for retrieving the amount of*/
   /* buffer space that is free within the input buffer.  The only      */
   /* parameter to this function is a pointer to the TTY Structure that */
   /* is associated with this TTY Device.  This function upon successful*/
   /* execution returns the number of bytes of space available within   */
   /* the input buffer on errors this function returns a negative error */
   /* code as defined in errno.h.                                       */
static int VSER_Device_Write_Room(struct tty_struct *TTYStruct)
{
   int           ret_val;
   DriverInfo_t *DriverInfoPtr;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameter that was passed in to make  */
   /* sure that it appears to be at least semi-valid.                   */
   if(TTYStruct)
   {
      /* The parameter passed in appears to be at least semi-valid.     */
      /* Retrieve the Driver Information for the driver via the driver  */
      /* data member of the TTY Structure.                              */
      DriverInfoPtr = (DriverInfo_t *)TTYStruct->driver_data;

      /* Next attempt to acquire the semaphore use to protect this      */
      /* Driver Information Entry.                                      */
      if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
      {
         /* Set the return value equal to the space available.          */
         ret_val = DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferLength;

         /* Release the semaphore used to protect the Driver Information*/
         /* Entry.                                                      */
         up(&DriverInfoPtr->DriverMutex);
      }
      else
         ret_val = -EINTR;
   }
   else
      ret_val = -EINVAL;

   DEBUGPRINT(KERN_ERR "%s : exit, ret_val = %d\n", __FUNCTION__, ret_val);

   return(ret_val);
}

   /* The following function returns the number of bytes which are      */
   /* currently within the input buffer.  The only parameter to this    */
   /* function is a pointer to the TTY Structure that is associated with*/
   /* this TTY Device.  This function upon successful execution returns */
   /* the number of filled bytes within the input buffer, on errors this*/
   /* function returns a negative error code as defined in errno.h.     */
static int VSER_Device_Characters_In_Buffer(struct tty_struct *TTYStruct)
{
   int           ret_val;
   DriverInfo_t *DriverInfoPtr;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameter that was passed in to make  */
   /* sure that it appears to be at least semi-valid.                   */
   if(TTYStruct)
   {
      /* The parameter passed in appears to be at least semi-valid.     */
      /* Retrieve the Driver Information for the driver via the driver  */
      /* data member of the TTY Structure.                              */
      DriverInfoPtr = (DriverInfo_t *)TTYStruct->driver_data;

      /* Next attempt to acquire the semaphore use to protect this      */
      /* Driver Information Entry.                                      */
      if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
      {
         /* Set the return value equal to the number of characters      */
         /* available in the buffer.                                    */
         ret_val = DriverInfoPtr->InputBufferLength;

         /* Release the semaphore used to protect the Driver Information*/
         /* Entry.                                                      */
         up(&DriverInfoPtr->DriverMutex);
      }
      else
         ret_val = -EINTR;
   }
   else
      ret_val = -EINVAL;

   DEBUGPRINT(KERN_ERR "%s : exit, ret_val = %d\n", __FUNCTION__, ret_val);

   return(ret_val);
}

   /* This function is responsible for flushing the input buffer. The   */
   /* only parameter to this function is a pointer to the TTY Structure */
   /* that is associated with this TTY Device.                          */
static void VSER_Device_Flush_Buffer(struct tty_struct *TTYStruct)
{
   DriverInfo_t     *DriverInfoPtr;
   struct tty_ldisc *TTYLDisc;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameter that was passed in to make  */
   /* sure that it appears to be at least semi-valid.                   */
   if(TTYStruct)
   {
      /* The parameter passed in appears to be at least semi-valid.     */
      /* Retrieve the Driver Information for the driver via the driver  */
      /* data member of the TTY Structure.                              */
      DriverInfoPtr = (DriverInfo_t *)TTYStruct->driver_data;

      /* Next attempt to acquire the semaphore use to protect this      */
      /* Driver Information Entry.                                      */
      if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
      {
         /* Reinitialize the buffer to invalidate all of the data which */
         /* might currently reside in it.                               */
         DriverInfoPtr->InputBufferLength        = 0;
         DriverInfoPtr->InputBufferReadPointer   = 0;
         DriverInfoPtr->InputBufferWritePointer  = 0;

         /* Since there is no longer any data to be read, we need to    */
         /* remove this Data Event (and the Data) from the Event Queue. */
         DriverInfoPtr->ReadLength               = 0;
         DriverInfoPtr->WaitEventMask           &= ~VSER_RECEIVE_DATA_EVENT;

         /* Release the semaphore used to protect the Driver Information*/
         /* Entry.                                                      */
         up(&DriverInfoPtr->DriverMutex);

         /* Now that we own no semaphores we can wake up anyone who     */
         /* maybe waiting to write data.                                */

         /* ** Note ** We can not own semaphores while making these     */
         /* calls because they may call the scheduler to reschedule the */
         /* current process.                                            */
         /* Check the flags to determine if the LDisc need to be called */
         /* to wake up.                                                 */
         if(TTYStruct->flags & (1 << TTY_DO_WRITE_WAKEUP))
         {
            if((TTYLDisc = tty_ldisc_ref_wait(TTYStruct)) != NULL)
            {
#if(LINUX_VERSION_CODE < KERNEL_VERSION(2,6,27))

               if(TTYLDisc->write_wakeup)
                  (*TTYLDisc->write_wakeup)(TTYStruct);

#else

               if(TTYLDisc->ops->write_wakeup)
                  (*TTYLDisc->ops->write_wakeup)(TTYStruct);

#endif
               tty_ldisc_deref(TTYLDisc);
            }
         }

         wake_up_interruptible(&TTYStruct->write_wait);
      }
   }

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

   /* The following function is responsible for processing the IOCTL    */
   /* requests for the TTY/Device Side of the driver.  The first        */
   /* parameter to this function is a pointer to the TTY Structure that */
   /* is associated with this TTY Device.  The second parameter to this */
   /* function is a pointer to the file structure associated with this  */
   /* device.  The third parameter to this function is the Command to be*/
   /* processed and the final parameter is the Parameter that maybe be  */
   /* associated with this command.  Note that the final parameter need */
   /* not be valid because not all commands specified or require the    */
   /* parameter to be used.  This function returns zero or a positive   */
   /* value upon success and a negative error code as defined in errno.h*/
   /* upon all errors.                                                  */
static int VSER_Device_Ioctl(struct tty_struct *TTYStruct, struct file *FilePointer, unsigned int Command, unsigned long Parameter)
{
   int                           ret_val;
   unsigned int                  DispatchEvent;
   unsigned int                  TempMask;
   DriverInfo_t                 *DriverInfoPtr;
   struct serial_icounter_struct OldInterruptCount;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if((TTYStruct) && (FilePointer))
   {
      /* Now determine if the IOCTL command that was specified is a     */
      /* command that is supported by this TTY device driver.           */
      switch(Command)
      {
         case TCSBRK:
         case TCSBRKP:
         case TIOCMGET:
         case TIOCMBIS:
         case TIOCMBIC:
         case TIOCMSET:
         case TIOCSERGETLSR:
         case TIOCMIWAIT:
         case TIOCGICOUNT:
            /* The command appears to be valid.  Retrieve the Driver    */
            /* Information for the driver via the driver data member of */
            /* the TTY Structure.                                       */
            DriverInfoPtr = (DriverInfo_t *)TTYStruct->driver_data;

            /* Next attempt to acquire the semaphore use to protect this*/
            /* Driver Information Entry.                                */
            if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
            {
               /* Now determine what type of command this is so the     */
               /* appropriate operation can be done.                    */
               switch(Command)
               {
                  case TCSBRK:
                  case TCSBRKP:
                     DEBUGPRINT(KERN_ERR "%s : Command TCSBRK, TCSBRKP. \n", __FUNCTION__);

                     /* Only inform the Transport Device of a Break if  */
                     /* there is a change in the Break State.           */
                     if(!(DriverInfoPtr->WaitEventMask & VSER_BREAK_EVENT))
                     {
                        /* Set the Break Wait Event Flag.               */
                        DriverInfoPtr->WaitEventMask |= VSER_BREAK_EVENT;

                        /* Release the semaphore used to protect the    */
                        /* Driver Information Entry.                    */
                        up(&DriverInfoPtr->DriverMutex);

                        /* Wake up the Wait Event Wait Queue.           */
                        wake_up_interruptible(&(DriverInfoPtr->WaitEventWaitQueue));
                     }
                     else
                     {
                        /* There is nothing to do other than to release */
                        /* the semaphore that we acquired earlier.      */
                        up(&DriverInfoPtr->DriverMutex);
                     }

                     /* Set the return value to indicate success.       */
                     ret_val = 0;
                     break;
                  case TIOCMGET:
                     DEBUGPRINT(KERN_ERR "%s : Command TIOCMGET. \n", __FUNCTION__);

                     /* Simply copy the current modem status into the   */
                     /* pointer supplied to us through the Parameter and*/
                     /* return with success to the caller.              */
                     ret_val = put_user(DriverInfoPtr->ModemStatusRegister, (unsigned int *)Parameter);

                     DEBUGPRINT(KERN_ERR "%s : Modem Status 0x%08X.\n", __FUNCTION__, DriverInfoPtr->ModemStatusRegister);

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);
                     break;
                  case TIOCMBIS:
                     DEBUGPRINT(KERN_ERR "%s : Command TIOCMBIS. \n", __FUNCTION__);

                     /* First get the bit mask passed in from user      */
                     /* space.                                          */
                     if(!(ret_val = get_user(TempMask, (unsigned int *)Parameter)))
                     {
                        /* Mask the input argument with the bits that   */
                        /* are valid to set.                            */
                        TempMask &= (VSER_MODEM_STATUS_RTS | VSER_MODEM_STATUS_DTR);

                        if((DriverInfoPtr->ModemStatusRegister & (VSER_MODEM_STATUS_RTS | VSER_MODEM_STATUS_DTR)) != TempMask)
                        {
                           /* Now OR the masked bits into the Modem     */
                           /* Status Register.                          */
                           DriverInfoPtr->ModemStatusRegister |= TempMask;

                           /* Set the Modem Status Wait Event Flag.     */
                           DriverInfoPtr->WaitEventMask       |= VSER_MODEM_STATUS_EVENT;

                           DispatchEvent                       = 1;
                        }
                        else
                           DispatchEvent                       = 0;
                     }
                     else
                        DispatchEvent = 0;

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);

                     /* Wake up the Wait Event Wait Queue (if there is  */
                     /* event to dispatch).                             */
                     if(DispatchEvent)
                        wake_up_interruptible(&(DriverInfoPtr->WaitEventWaitQueue));
                     break;
                  case TIOCMBIC:
                     DEBUGPRINT(KERN_ERR "%s : Command TIOCMBIC. \n", __FUNCTION__);

                     /* First get the bit mask passed in from user      */
                     /* space.                                          */
                     if(!(ret_val = get_user(TempMask, (unsigned int *)Parameter)))
                     {
                        /* Mask the input argument with the bits that   */
                        /* are valid to set.                            */
                        TempMask &= (VSER_MODEM_STATUS_RTS | VSER_MODEM_STATUS_DTR);

                        if((DriverInfoPtr->ModemStatusRegister & (VSER_MODEM_STATUS_RTS | VSER_MODEM_STATUS_DTR)) != TempMask)
                        {
                           /* Now MASK the masked bits into the Modem   */
                           /* Status Register to clear the specified    */
                           /* bits.                                     */
                           DriverInfoPtr->ModemStatusRegister &= ~TempMask;

                           /* Set the Modem Status Wait Event Flag.     */
                           DriverInfoPtr->WaitEventMask       |= VSER_MODEM_STATUS_EVENT;

                           DispatchEvent                       = 1;
                        }
                        else
                           DispatchEvent                       = 0;
                     }
                     else
                        DispatchEvent = 0;

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);

                     /* Wake up the Wait Event Wait Queue (if there is  */
                     /* event to dispatch).                             */
                     if(DispatchEvent)
                        wake_up_interruptible(&(DriverInfoPtr->WaitEventWaitQueue));
                     break;
                  case TIOCMSET:
                     DEBUGPRINT(KERN_ERR "%s : Command TIOCMSET. \n", __FUNCTION__);

                     /* First get the bit mask passed in from user      */
                     /* space.                                          */
                     if(!(ret_val = get_user(TempMask, (unsigned int *)Parameter)))
                     {
                        /* Mask the input argument with the bits that   */
                        /* are valid to set.                            */
                        TempMask &= (VSER_MODEM_STATUS_RTS | VSER_MODEM_STATUS_DTR);

                        if((DriverInfoPtr->ModemStatusRegister & (VSER_MODEM_STATUS_RTS | VSER_MODEM_STATUS_DTR)) != TempMask)
                        {
                           /* Mask the bits out of the Modem Status     */
                           /* Register.                                 */
                           DriverInfoPtr->ModemStatusRegister &= ~(VSER_MODEM_STATUS_RTS | VSER_MODEM_STATUS_DTR);

                           /* Now set the bits that were specified.     */
                           DriverInfoPtr->ModemStatusRegister |= TempMask;

                           /* Set the Modem Status Wait Event Flag.     */
                           DriverInfoPtr->WaitEventMask       |= VSER_MODEM_STATUS_EVENT;

                           DispatchEvent                       = 1;
                        }
                        else
                           DispatchEvent                       = 0;
                     }
                     else
                        DispatchEvent = 0;

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);

                     /* Wake up the Wait Event Wait Queue (if there is  */
                     /* event to dispatch).                             */
                     if(DispatchEvent)
                        wake_up_interruptible(&(DriverInfoPtr->WaitEventWaitQueue));
                     break;
                  case TIOCSERGETLSR:
                     DEBUGPRINT(KERN_ERR "%s : Command TIOCSERGETLSR. \n", __FUNCTION__);

                     /* Simply copy the current line status into the    */
                     /* pointer supplied to us through the Parameter and*/
                     /* return with success to the caller.              */
                     ret_val = put_user(DriverInfoPtr->LineStatusRegister, (unsigned int *)Parameter);

                     /* Reset the Line Status after being read, this is */
                     /* to emulate hardware.                            */
                     DriverInfoPtr->LineStatusRegister = 0;

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);
                     break;
                  case TIOCMIWAIT:
                     DEBUGPRINT(KERN_ERR "%s : Command TIOCMWAIT. \n", __FUNCTION__);

                     /* Mask the input argument with the bits that are  */
                     /* valid to wait on.                               */
                     Parameter &= (VSER_MODEM_STATUS_CTS | VSER_MODEM_STATUS_CD | VSER_MODEM_STATUS_RNG | VSER_MODEM_STATUS_DSR);

                     /* Save the Interrupt Count to check against for   */
                     /* changes.                                        */
                     OldInterruptCount = DriverInfoPtr->InterruptCount;

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);

                     /* Check to see if one of the specified events has */
                     /* occurred.                                       */
                     ret_val = wait_event_interruptible(DriverInfoPtr->ModemStatusWaitQueue, (((Parameter & VSER_MODEM_STATUS_CTS) && (OldInterruptCount.cts != DriverInfoPtr->InterruptCount.cts)) || ((Parameter & VSER_MODEM_STATUS_CD) && (OldInterruptCount.dcd != DriverInfoPtr->InterruptCount.dcd)) || ((Parameter & VSER_MODEM_STATUS_RNG) && (OldInterruptCount.rng != DriverInfoPtr->InterruptCount.rng)) || ((Parameter & VSER_MODEM_STATUS_DSR) && (OldInterruptCount.dsr != DriverInfoPtr->InterruptCount.dsr)) || (DriverInfoPtr->ShutDown)));
                     break;
                  case TIOCGICOUNT:
                     DEBUGPRINT(KERN_ERR "%s : Command TIOCGICOUNT. \n", __FUNCTION__);

                     /* Copy the current Interrupt Counter structure    */
                     /* into the pointer which was passed in.           */
                     if(copy_to_user((void *)Parameter, &DriverInfoPtr->InterruptCount, sizeof(struct serial_icounter_struct)))
                        ret_val = -EFAULT;
                     else
                        ret_val = 0;

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);
                     break;
                  default:
                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);

                     ret_val = -ENOIOCTLCMD;
                     break;
               }
            }
            else
               ret_val = -EINTR;
            break;
         default:
            DEBUGPRINT(KERN_ERR "%s : Unhandled Command %u.\n", __FUNCTION__, Command);

            ret_val = -ENOIOCTLCMD;
            break;
      }
   }
   else
      ret_val = -EINVAL;

   DEBUGPRINT(KERN_ERR "%s : exit, ret_val = %d\n", __FUNCTION__, ret_val);

   return(ret_val);
}

static void VSER_Device_Set_Termios(struct tty_struct *TTYStruct, struct ktermios *OldAttributes)
{
   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

static void VSER_Device_Throttle(struct tty_struct *TTYStruct)
{
   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

static void VSER_Device_UnThrottle(struct tty_struct *TTYStruct)
{
   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

static void VSER_Device_Stop(struct tty_struct *TTYStruct)
{
   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

static void VSER_Device_Start(struct tty_struct *TTYStruct)
{
   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

static void VSER_Device_HangUp(struct tty_struct *TTYStruct)
{
   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

   /* This function is responsible for turning on and off the break     */
   /* condition.  If this function is implemented the high-level tty    */
   /* driver will handle the TCSBKRP, TCSBRKP, TIOCSBRK, and TIOCCBRK   */
   /* ioctls.  Otherwise, these ioctls will be passed down to this      */
   /* driver to handle.  The first parameter to this function is a      */
   /* pointer to the TTY Structure that is associated with this TTY     */
   /* Device.  The second parameter to this function is the state of the*/
   /* Break condition, -1 break on, 0 break off.                        */
#if(LINUX_VERSION_CODE < KERNEL_VERSION(2,6,27))
   static void VSER_Device_Break_Control(struct tty_struct *TTYStruct, int State)
#else
   static int VSER_Device_Break_Control(struct tty_struct *TTYStruct, int State)
#endif
{
   DriverInfo_t *DriverInfoPtr;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if(TTYStruct)
   {
      /* The parameters passed in appear to be at least semi-valid.     */
      /* Retrieve the Driver Information for the driver via the driver  */
      /* data member of the TTY Structure.                              */
      DriverInfoPtr = (DriverInfo_t *)TTYStruct->driver_data;

      /* Next attempt to acquire the semaphore use to protect this      */
      /* Driver Information Entry.                                      */
      if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
      {
         /* Check to see if this is a request to turn on the break      */
         /* condition.                                                  */
         if(State == -1)
         {
            /* Only inform the Transport Device of a Break if there is  */
            /* a change in the Break State.                             */
            if(!(DriverInfoPtr->WaitEventMask & VSER_BREAK_EVENT))
            {
               /* This was a request to turn on the break condition, set*/
               /* the flag in the wait event mask.                      */
               DriverInfoPtr->WaitEventMask |= VSER_BREAK_EVENT;

               /* Release the semaphore used to protect the Driver      */
               /* Information Entry.                                    */
               up(&DriverInfoPtr->DriverMutex);

               /* Wake up the Wait Event Wait Queue.                    */
               wake_up_interruptible(&(DriverInfoPtr->WaitEventWaitQueue));
            }
            else
            {
               /* There is nothing to do other than to release the      */
               /* semaphore that we acquired earlier.                   */
               up(&DriverInfoPtr->DriverMutex);
            }
         }
         else
         {
            /* This was a request to turn off the break condition.      */
            /* Release the semaphore used to protect the Driver         */
            /* Information Entry.                                       */
            up(&DriverInfoPtr->DriverMutex);
         }
      }
   }

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);

#if(LINUX_VERSION_CODE >= KERNEL_VERSION(2,6,27))
   return(0);
#endif
}

static void VSER_Device_Wait_Until_Sent(struct tty_struct *TTYStruct, int Timeout)
{
   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

static void VSER_Device_Send_XCharacter(struct tty_struct *TTYStruct, char Character)
{
   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   DEBUGPRINT(KERN_ERR "%s : exit\n", __FUNCTION__);
}

   /* The following function is responsible for opening the transport   */
   /* side of the Virtual Serial Port Driver.  The first parameter to   */
   /* this function is a pointer to the inode structure that is         */
   /* associated with this character device file. The second parameter  */
   /* to this function is a pointer to the file structure associated    */
   /* with this driver.  This function returns zero upon successful     */
   /* execution or a negative error code as defined in errno.h upon all */
   /* errors.                                                           */
static int VSER_Transport_Open(struct inode *INode, struct file *FilePointer)
{
   int           ret_val;
   DriverInfo_t *DriverInfoPtr;

   DEBUGPRINT(KERN_ERR "%s : enter, Minor = %u\n", __FUNCTION__, MINOR(INode->i_rdev));

   /* Increment the Module Use Count.                                   */
   try_module_get(THIS_MODULE);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if((INode) && (FilePointer))
   {
      /* The parameters passed in appear to be at least semi-valid.  Now*/
      /* note the driver information structure associated with this     */
      /* Minor Device Number.                                           */
      DriverInfoPtr = &DriverInfoList[MINOR(INode->i_rdev)];

      /* Check to see if the device is already opened.  If so, then     */
      /* refuse the Open Request.                                       */
      if(!DriverInfoPtr->TransportOpen)
      {
         /* Next attempt to acquire the semaphore use to protect this   */
         /* Driver Information Entry.                                   */
         if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
         {
            /* Initialize the return value to indicate success.         */
            ret_val = 0;

            /* Check to see if the input buffer has been allocated for  */
            /* this driver entry.                                       */
            if(!DriverInfoPtr->InputBuffer)
            {
               /* The Driver Input Buffer has not been allocated so     */
               /* allocate an input buffer for the driver.              */
               if((DriverInfoPtr->InputBuffer = (unsigned char *)kmalloc((VSER_DEVICE_INPUT_BUFFER_SIZE*sizeof(unsigned char)), GFP_KERNEL)) != NULL)
               {
                  /* The Input Buffer was successfully allocated, now   */
                  /* initialize the input buffer parameters.            */
                  DriverInfoPtr->InputBufferLength       = 0;
                  DriverInfoPtr->InputBufferSize         = VSER_DEVICE_INPUT_BUFFER_SIZE;
                  DriverInfoPtr->InputBufferReadPointer  = 0;
                  DriverInfoPtr->InputBufferWritePointer = 0;
               }
               else
                  ret_val = -ENOMEM;
            }

            /* Check the return value to see if everything has been     */
            /* successful up to this point.                             */
            if(!ret_val)
            {
               /* Everything is ok so far, now initialize the remaining */
               /* items pertaining to the Transport Side of the Driver  */
               /* in the Driver Information Entry.                      */
               DriverInfoPtr->TransportOpen = 1;
               DriverInfoPtr->ShutDown      = 0;
               DriverInfoPtr->WaitEventMask = 0;

               /* Reset the Current Read Length.                        */
               DriverInfoPtr->ReadLength    = 0;

               /* Finally set the private data in the File Information  */
               /* Structure to point to this entry for use with the rest*/
               /* of the functions associated with this driver.         */
               FilePointer->private_data = (void *)DriverInfoPtr;
            }

            /* Release the semaphore used to protect the Driver         */
            /* Information Entry.                                       */
            up(&DriverInfoPtr->DriverMutex);
         }
         else
            ret_val = -ENODEV;
      }
      else
         ret_val = -EACCES;
   }
   else
      ret_val = -EINVAL;

   /* Check to see if an error occurred, if so decrement the Module Use */
   /* Count.                                                            */
   if(ret_val)
   {
      /* Decrement the use count of the module                          */
      module_put(THIS_MODULE);
   }

   DEBUGPRINT(KERN_ERR "%s : exit, ret_val = %d\n", __FUNCTION__, ret_val);

   return(ret_val);
}

   /* The following function is responsible for closing the transport   */
   /* side of the Virtual Serial Port Driver.  Note that the memory     */
   /* associated with this device is only freed if both sides of the    */
   /* driver are closed. The first parameter to this function is a      */
   /* pointer to the inode structure that is associated with this       */
   /* character device.  The second parameter to this function is a     */
   /* pointer to the file structure associated with this this device.   */
   /* This function returns zero upon successful execution or a negative*/
   /* error code as defined in errno.h upon all errors.                 */
static int VSER_Transport_Close(struct inode *INode, struct file *FilePointer)
{
   int           ret_val;
   DriverInfo_t *DriverInfoPtr;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if((INode) && (FilePointer))
   {
      /* The parameters passed in appear to be at least semi-valid.  Now*/
      /* note the driver information structure associated with this     */
      /* Minor Device Number.                                           */
      DriverInfoPtr = &DriverInfoList[MINOR(INode->i_rdev)];

      /* Next attempt to acquire the semaphore use to protect this      */
      /* Driver Information Entry.                                      */
      if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
      {
         /* The semaphore protecting this entry was acquired            */
         /* successfully, if either the Device Side of the Driver       */
         /* remains open or it doesn't, change the state of the Device  */
         /* side of the Driver and reset the driver data member of the  */
         /* TTY Structure.                                              */
         DriverInfoPtr->TransportOpen = 0;
         FilePointer->private_data    = NULL;

         /* The semaphore protecting this entry was acquired            */
         /* successfully, now check to see if the device side of the    */
         /* driver remains open.                                        */
         if(!DriverInfoPtr->DeviceOpen)
         {
            /* The device side of the driver is not open so clean up the*/
            /* driver.  Set the shut down flag here after releasing the */
            /* semaphore wake up anyone waiting in the wait queue.      */
            DriverInfoPtr->ShutDown    = 1;

            /* Next free the memory allocated for the input buffer.     */
            kfree(DriverInfoPtr->InputBuffer);

            DriverInfoPtr->InputBuffer = NULL;
         }

         /* Release the semaphore used to protect the Driver Information*/
         /* Entry.                                                      */
         up(&DriverInfoPtr->DriverMutex);

         /* Wake up anyone who may have been waiting now that we don't  */
         /* own any semaphores.                                         */
         wake_up_interruptible(&(DriverInfoPtr->ModemStatusWaitQueue));
         wake_up_interruptible(&(DriverInfoPtr->WaitEventWaitQueue));

         /* Set the return value to indicate success.                   */
         ret_val = 0;
      }
      else
         ret_val = -EINTR;
   }
   else
      ret_val = -EINVAL;

   /* Decrement the Module Use Count.                                   */
   if(!ret_val)
   {
      /* Decrement the use count of the module                          */
      module_put(THIS_MODULE);
   }

   DEBUGPRINT(KERN_ERR "%s : exit, ret_val = %d\n", __FUNCTION__, ret_val);

   return(ret_val);
}

   /* The following function is responsible for reading data from the   */
   /* input buffer associated with this Virtual Serial Port Driver.  The*/
   /* input buffer that this function reads from is the buffer which the*/
   /* TTY Device writes to.  The first parameter to this function is a  */
   /* pointer to the file structure associated with this device.  The   */
   /* second parameter to this function is a pointer to the buffer in   */
   /* which to read the data into.  The third parameter is the length of*/
   /* the buffer which the previous parameter points to.  The final     */
   /* parameter is the offset within the file.  This function returns   */
   /* the number of bytes which were successfully read or a negative    */
   /* error code as defined in errno.h upon all errors.                 */
static ssize_t VSER_Transport_Read(struct file *FilePointer, char *Buffer, size_t Length, loff_t *LongOffset)
{
   int                ret_val;
   unsigned int       DeviceOpen;
   unsigned int       TempLength;
   DriverInfo_t      *DriverInfoPtr;
   unsigned char     *TempBufferPtr;
   struct tty_ldisc  *TTYLDisc;
   struct tty_struct *TTYStruct;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if((FilePointer) && (Buffer))
   {
      /* The parameters passed in appear to be at least semi-valid.  Now*/
      /* make sure that this is a request to read at least some data.   */
      if(Length)
      {
         /* This is a request to read at least some data, retrieve the  */
         /* Driver Information for the driver via the driver data member*/
         /* of the TTY Structure.                                       */
         DriverInfoPtr = (DriverInfo_t *)FilePointer->private_data;

         DEBUGPRINT(KERN_ERR "DriverInfoPtr %p Buffer %p Length %d\n", DriverInfoPtr, Buffer, Length);

         /* Next attempt to acquire the semaphore use to protect this   */
         /* Driver Information Entry.                                   */
         if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
         {
            /* Make sure that there is Data to Read in the Input Buffer.*/
            if(DriverInfoPtr->InputBufferLength)
            {
               /* First let's calculate exactly how much data we are    */
               /* actually going to read.                               */
               TempLength = DriverInfoPtr->InputBufferLength;

               /* If the Amount of Data in the Buffer is larger that the*/
               /* Buffer Size, then we will only read up to the Buffer  */
               /* Size.                                                 */
               if(TempLength > (unsigned int)Length)
                  TempLength = Length;

               /* Initialize the return value.                          */
               ret_val = 0;

               /* Now we will check to see if our Circular Buffer can be*/
               /* copied in a single Memory Copy (i.e.  it will NOT     */
               /* wrap).                                                */
               if((DriverInfoPtr->InputBufferReadPointer + TempLength) > DriverInfoPtr->InputBufferSize)
               {
                  /* Copy the data up to the end of the Buffer.         */
                  if(!copy_to_user(Buffer, &(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferReadPointer]), (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferReadPointer)))
                  {
                     DEBUGPRINT(KERN_ERR "&(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferReadPointer]) %p\n", &(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferReadPointer]));

                     /* Now Copy the data starting at the beginning of  */
                     /* the Buffer.                                     */
                     if(!copy_to_user(&(Buffer[(DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferReadPointer)]), &(DriverInfoPtr->InputBuffer[0]), (TempLength - (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferReadPointer))))
                     {
                        DEBUGPRINT(KERN_ERR "&(DriverInfoPtr->InputBuffer[0]) %p\n", &(DriverInfoPtr->InputBuffer[0]));
                     }
                  }
                  else
                     ret_val = -EFAULT;
               }
               else
               {
                  /* The buffer will not wrap, so simply copy the data  */
                  /* into the Buffer.                                   */
                  if(copy_to_user(Buffer, &(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferReadPointer]), TempLength))
                     ret_val = -EFAULT;

                  DEBUGPRINT(KERN_ERR "&(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferReadPointer]) %p\n",  &(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferReadPointer]));
               }

               /* Check to see if the copy was performed successfully.  */
               if(!ret_val)
               {
                  /* Increment the Data Read Pointer and the amount data*/
                  /* present in the Input Buffer.                       */
                  DriverInfoPtr->InputBufferLength      -= TempLength;
                  DriverInfoPtr->InputBufferReadPointer += TempLength;

                  /* Wrap the Input Buffer Read Pointer (if needed).    */
                  if(DriverInfoPtr->InputBufferReadPointer >= DriverInfoPtr->InputBufferSize)
                     DriverInfoPtr->InputBufferReadPointer -= DriverInfoPtr->InputBufferSize;

                  /* Inform the caller of the number of Bytes that were */
                  /* read.                                              */
                  ret_val                                = TempLength;

                  /* Now that some data has been read out of the buffer */
                  /* check to see if the buffer needs to be shrank to   */
                  /* free up some memory.  Check to see if the size of  */
                  /* the buffer is currently greater then the minimum   */
                  /* buffer size.                                       */
                  if(DriverInfoPtr->InputBufferSize > VSER_DEVICE_INPUT_BUFFER_SIZE)
                  {
                     /* The Input Buffer Size is currently greater then */
                     /* the Minimum Buffer size, therefore there is a   */
                     /* possibility that it needs to be shrank.  Check  */
                     /* to see if the amount of data in the buffer is   */
                     /* less then 1/4 of the buffer size.               */
                     if(DriverInfoPtr->InputBufferLength < (DriverInfoPtr->InputBufferSize/4))
                     {
                        /* The amount of data in the buffer is less then*/
                        /* 1/4 of the buffer size.  Allocate a new      */
                        /* buffer of 1/2 the current size of the buffer,*/
                        /* this will shrink the memory usage but still  */
                        /* allow room for the buffer to grow if         */
                        /* necessary.                                   */
                        if((TempBufferPtr = (unsigned char *)kmalloc(DriverInfoPtr->InputBufferSize/2, GFP_KERNEL)) != NULL)
                        {
                           /* The new buffer has been successfully      */
                           /* allocated, Now we will check to see if our*/
                           /* Circular Buffer can be copied in a single */
                           /* Memory Copy (i.e.  it will NOT wrap).     */
                           if((DriverInfoPtr->InputBufferReadPointer + DriverInfoPtr->InputBufferLength) > DriverInfoPtr->InputBufferSize)
                           {
                              /* Copy the data up to the end of the     */
                              /* Buffer.                                */
                              memcpy(TempBufferPtr, &(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferReadPointer]), (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferReadPointer));

                              /* Now Copy the data starting at the      */
                              /* beginning of the Buffer.               */
                              memcpy(&(TempBufferPtr[(DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferReadPointer)]), &(DriverInfoPtr->InputBuffer[0]), (DriverInfoPtr->InputBufferLength - (DriverInfoPtr->InputBufferSize - DriverInfoPtr->InputBufferReadPointer)));
                           }
                           else
                           {
                              /* The buffer will not wrap, so simply    */
                              /* copy the data into the Buffer.         */
                              memcpy(TempBufferPtr, &(DriverInfoPtr->InputBuffer[DriverInfoPtr->InputBufferReadPointer]), DriverInfoPtr->InputBufferLength);
                           }

                           /* The buffer has been copied, adjust the    */
                           /* Input Buffer Read Pointer, the Input      */
                           /* Buffer Write Pointer and the Input Buffer */
                           /* Size.                                     */
                           DriverInfoPtr->InputBufferSize         = DriverInfoPtr->InputBufferSize/2;
                           DriverInfoPtr->InputBufferReadPointer  = 0;
                           DriverInfoPtr->InputBufferWritePointer = DriverInfoPtr->InputBufferLength;

                           /* Finally free the previous smaller Input   */
                           /* Buffer and set the Temp Buffer Pointer to */
                           /* the Input Buffer Pointer.                 */
                           kfree(DriverInfoPtr->InputBuffer);

                           DriverInfoPtr->InputBuffer = TempBufferPtr;
                        }
                     }
                  }
               }
            }
            else
               ret_val = 0;

            /* Save the state of the Device Side while still protected  */
            /* by the semaphore, also save the pointer to the TTY       */
            /* Structure.                                               */
            DeviceOpen = DriverInfoPtr->DeviceOpen;
            TTYStruct  = DriverInfoPtr->TTYStruct;
            TempLength = DriverInfoPtr->InputBufferLength;

            /* Release the semaphore used to protect the Driver         */
            /* Information Entry.                                       */
            up(&DriverInfoPtr->DriverMutex);

            /* Check to see if the device side is open to see if any    */
            /* writers need to be woken up.  However, only attempt to   */
            /* wake up writers if all of the data has been read from the*/
            /* buffer.                                                  */
            if((DeviceOpen) && (TTYStruct) && (ret_val > 0) && (!TempLength))
            {
               /* Now that we own no semaphores we can wake up anyone   */
               /* who maybe waiting to write data.                      */

               /* ** Note ** We can not own semaphores while making     */
               /* these calls because they may call the scheduler to    */
               /* reschedule the current process.                       */

               /* Check the flags to determine if the LDisc need to be  */
               /* called to wake up.                                    */
               if(TTYStruct->flags & (1 << TTY_DO_WRITE_WAKEUP))
               {
                  if((TTYLDisc = tty_ldisc_ref_wait(TTYStruct)) != NULL)
                  {
#if(LINUX_VERSION_CODE < KERNEL_VERSION(2,6,27))

                     if(TTYLDisc->write_wakeup)
                        (*TTYLDisc->write_wakeup)(TTYStruct);

#else

                     if(TTYLDisc->ops->write_wakeup)
                        (*TTYLDisc->ops->write_wakeup)(TTYStruct);

#endif
                     tty_ldisc_deref(TTYLDisc);
                  }
               }

               wake_up_interruptible(&TTYStruct->write_wait);
            }
         }
         else
            ret_val = -EINTR;
      }
      else
         ret_val = 0;
   }
   else
      ret_val = -EINVAL;

   DEBUGPRINT(KERN_ERR "%s : exit, ret_val = %d\n", __FUNCTION__, ret_val);

   return(ret_val);
}

   /* The following function is responsible for writing data to the     */
   /* buffer which exists within the TTY device associated with this    */
   /* Virtual Serial Port Driver.  The first parameter to this function */
   /* is a pointer to the file structure associated with this device.   */
   /* The second parameter is a pointer to a buffer of characters which */
   /* is to be written.  The third parameter is the number of characters*/
   /* within the buffer pointed to by the previous parameter.  The final*/
   /* parameter is the offset within the file.  This function returns   */
   /* the number of bytes which were successfully written or a negative */
   /* error code as defined in errno.h upon all errors.                 */
static ssize_t VSER_Transport_Write(struct file *FilePointer, const char *Buffer, size_t Length, loff_t *LongOffset)
{
   int                ret_val;
   unsigned int       DeviceOpen;
   int                LenAvail;
   DriverInfo_t      *DriverInfoPtr;
   struct tty_struct *TTYStruct;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if((FilePointer) && (Buffer))
   {
      /* The parameters passed in appear to be at least semi-valid.  Now*/
      /* make sure that there is at least some data to be written.      */
      if(Length)
      {
         /* There is at least some data to be written, retrieve the     */
         /* Driver Information for the driver via the driver data member*/
         /* of the TTY Structure.                                       */
         DriverInfoPtr = (DriverInfo_t *)FilePointer->private_data;

         /* Next attempt to acquire the semaphore use to protect this   */
         /* Driver Information Entry.                                   */
         if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
         {
            /* Check to see if the Device side of the driver is open.   */
            if(DriverInfoPtr->DeviceOpen)
            {
               /* Ask interface how much room we have for data          */
               LenAvail = tty_buffer_request_room(DriverInfoPtr->TTYStruct, Length);

               /* Now insert however much we have room to buffer        */
               tty_insert_flip_string(DriverInfoPtr->TTYStruct, Buffer, LenAvail);

               /* Adjust the Receive Interrupt Count by the number of   */
               /* Bytes successfully placed into the Devices Receive    */
               /* Buffer.                                               */
               DriverInfoPtr->InterruptCount.rx            += LenAvail;

               /* Set the return value to the number of bytes written   */
               /* to the input buffer.                                  */
               ret_val                                      = LenAvail;
            }
            else
            {
               /* The Device side of the driver is not open so return   */
               /* that all of the data was written but do not write any */
               /* data.                                                 */
               ret_val = Length;
            }

            /* Save the status of the Device Open Flag to a local       */
            /* variable so that the state can be check to determine if a*/
            /* tty_schedule_flip needs to be performed.  Also save the  */
            /* pointer to the TTY Structure.                            */
            DeviceOpen = DriverInfoPtr->DeviceOpen;
            TTYStruct  = DriverInfoPtr->TTYStruct;

            /* Release the semaphore used to protect the Driver         */
            /* Information Entry.                                       */
            up(&DriverInfoPtr->DriverMutex);

            /* Now that we do not own the semaphore call the function   */
            /* used to inform the TTY device that new data has been put */
            /* into the buffer.  Note that this is only done if the     */
            /* device is open and same data was actual written to the   */
            /* buffer.                                                  */
            if((DeviceOpen) && (TTYStruct) && (ret_val))
               tty_flip_buffer_push(TTYStruct);
         }
         else
            ret_val = -EINTR;
      }
      else
         ret_val = 0;
   }
   else
      ret_val = -EINVAL;

   DEBUGPRINT(KERN_ERR "%s : exit, ret_val = %d\n", __FUNCTION__, ret_val);

   return(ret_val);
}

   /* The following function is responsible for processing the IOCTL    */
   /* requests for the Transport side of the Driver.  The first         */
   /* parameter to this function is a pointer to the inode structure    */
   /* that is associated with this character device.  The second        */
   /* parameter to this function is a pointer to the file structure     */
   /* associated with this device.  The third parameter to this function*/
   /* is the Command to be processed and the final parameter is the     */
   /* Parameter that maybe be associated with this command.  Note that  */
   /* the final parameter need not be valid because not all commands    */
   /* specified or require the parameter to be used.  This function     */
   /* returns zero or a positive value upon success and a negative error*/
   /* code as defined in errno.h upon all errors.                       */
static int VSER_Transport_Ioctl(struct inode *INode, struct file *FilePointer, unsigned int Command, unsigned long Parameter)
{
   int            ret_val;
   unsigned int   ModemStatusChange;
   unsigned int   FlipPush;
   DriverInfo_t  *DriverInfoPtr;
   unsigned long  WaitEventMask;

   DEBUGPRINT(KERN_ERR "%s : enter\n", __FUNCTION__);

   /* Before proceeding check the parameters that were passed in to make*/
   /* sure that they appear to be at least semi-valid.                  */
   if((INode) && (FilePointer))
   {
      /* Now determine if the IOCTL command that was specified is a     */
      /* command that is supported by this character device.            */
      switch(Command)
      {
         case VSER_IOCTL_SEND_BREAK:
         case VSER_IOCTL_GET_BREAK_STATUS:
         case VSER_IOCTL_GET_MODEM_STATUS:
         case VSER_IOCTL_SET_MODEM_STATUS:
         case VSER_IOCTL_GET_LINE_STATUS:
         case VSER_IOCTL_SET_LINE_STATUS:
         case VSER_IOCTL_WAIT_SERIAL_EVENT:
         case VSER_IOCTL_RECEIVE_DATA_LENGTH:
            /* The IOCTL specified is supported by this device now      */
            /* retrieve the Driver Information for the driver via the   */
            /* driver data member of the TTY Structure.                 */
            DriverInfoPtr = (DriverInfo_t *)FilePointer->private_data;

            /* Next attempt to acquire the semaphore use to protect this*/
            /* Driver Information Entry.                                */
            if((DriverInfoPtr) && (!down_interruptible(&DriverInfoPtr->DriverMutex)))
            {
               /* Now determine what command this is so the appropriate */
               /* operation can be done.                                */
               switch(Command)
               {
                  case VSER_IOCTL_SEND_BREAK:
                     DEBUGPRINT(KERN_ERR "%s : Command VSER_IOCTL_SEND_BREAK.\n", __FUNCTION__);

                     /* This was a command to send a break to the Device*/
                     /* side of the driver.  First, initialize the Flip */
                     /* Push variable to FALSE to indicate that a TTY   */
                     /* Flip Push does not need to be done.             */
                     FlipPush = 0;

                     /* Now add the break indication to the TTY's       */
                     /* receive buffer.                                 */
                     if(tty_insert_flip_char(DriverInfoPtr->TTYStruct, '\0', TTY_BREAK))
                     {
                        /* Character was added to the buffer            */
                        /* Change Flip Push to indicate that one needs  */
                        /* to be done.                                  */
                        FlipPush = 1;
                     }

                     /* Now Increment the break Interrupt Count.        */
                     DriverInfoPtr->InterruptCount.brk++;

                     /* Set the Break Indication bit in the Line Status */
                     /* Register.                                       */
                     DriverInfoPtr->LineStatusRegister |= VSER_LINE_STATUS_BREAK_INDICATION;

                     ret_val = 0;

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);

                     /* Check to see if we need to push the file input  */
                     /* buffer.                                         */
                     if(FlipPush)
                     {
                        /* Push the file input buffer to inform the TTY */
                        /* device that new data has been put into the   */
                        /* buffer.                                      */
                        tty_schedule_flip(DriverInfoPtr->TTYStruct);
                     }
                     break;
                  case VSER_IOCTL_GET_BREAK_STATUS:
                     DEBUGPRINT(KERN_ERR "%s : Command VSER_IOCTL_GET_BREAK.\n", __FUNCTION__);

                     /* Set the return value to indicate success.       */
                     ret_val                       = 0;

                     /* Reset the Wait Event Mask bit for Break Events  */
                     /* to indicate that the Break Event has been       */
                     /* received and cleared.                           */
                     DriverInfoPtr->WaitEventMask &= ~VSER_BREAK_EVENT;

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);
                     break;
                  case VSER_IOCTL_GET_MODEM_STATUS:
                     DEBUGPRINT(KERN_ERR "%s : Command VSER_IOCTL_GET_MODEM_STATUS.\n", __FUNCTION__);

                     /* Simply copy the current modem status into the   */
                     /* pointer supplied through the Parameter and      */
                     /* return with success to the caller.              */
                     ret_val = put_user(DriverInfoPtr->ModemStatusRegister, (unsigned int *)Parameter);

                     /* Check to make sure that the Modem Status        */
                     /* Register was successfully stored into the       */
                     /* Pointer passed in from user space.              */
                     if(!ret_val)
                     {
                        /* Reset the Wait Event Mask bit for Modem      */
                        /* Status Events to indicate that the Modem     */
                        /* Status Register has been read.               */
                        DriverInfoPtr->WaitEventMask &= ~VSER_MODEM_STATUS_EVENT;
                     }

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);
                     break;
                  case VSER_IOCTL_SET_MODEM_STATUS:
                     DEBUGPRINT(KERN_ERR "%s : Command VSER_IOCTL_SET_MODEM_STATUS.\n", __FUNCTION__);

                     /* Initialize the modem status change flag to      */
                     /* false.                                          */
                     ModemStatusChange = 0;

                     /* Mask the input argument with the bits that are  */
                     /* valid to set.                                   */
                     Parameter &= (VSER_MODEM_STATUS_CTS | VSER_MODEM_STATUS_CD | VSER_MODEM_STATUS_RNG | VSER_MODEM_STATUS_DSR);

                     /* Determine if this is a request to change the    */
                     /* state of CTS.                                   */
                     if(((Parameter & VSER_MODEM_STATUS_CTS) && (!(DriverInfoPtr->ModemStatusRegister & VSER_MODEM_STATUS_CTS))) || (!(Parameter & VSER_MODEM_STATUS_CTS) && (DriverInfoPtr->ModemStatusRegister & VSER_MODEM_STATUS_CTS)))
                     {
                        /* This is a request to either change CTS from  */
                        /* high to low or low to high.  In this case    */
                        /* increment the interrupt counter.             */
                        DriverInfoPtr->InterruptCount.cts++;

                        ModemStatusChange = 1;
                     }

                     /* Determine if this is a request to change the    */
                     /* state of CD.                                    */
                     if(((Parameter & VSER_MODEM_STATUS_CD) && (!(DriverInfoPtr->ModemStatusRegister & VSER_MODEM_STATUS_CD))) || (!(Parameter & VSER_MODEM_STATUS_CD) && (DriverInfoPtr->ModemStatusRegister & VSER_MODEM_STATUS_CD)))
                     {
                        /* This is a request to either change CD from   */
                        /* high to low or low to high.  In this case    */
                        /* increment the interrupt counter.             */
                        DriverInfoPtr->InterruptCount.dcd++;

                        ModemStatusChange = 1;
                     }

                     /* Determine if this is a request to change the    */
                     /* state of the RNG.                               */
                     if(((Parameter & VSER_MODEM_STATUS_RNG) && (!(DriverInfoPtr->ModemStatusRegister & VSER_MODEM_STATUS_RNG))))
                     {
                        /* This is a request to change the RNG from low */
                        /* to high.  In this case increment the         */
                        /* interrupt counter.  Note that the interrupt  */
                        /* counter is intentional not incremented in the*/
                        /* case of a high to low transition, this is to */
                        /* emulate the exact functionality of the serial*/
                        /* driver implemented in serial.c.              */
                        DriverInfoPtr->InterruptCount.rng++;

                        ModemStatusChange = 1;
                     }

                     /* Determine if this is a request to change the    */
                     /* state of DSR.                                   */
                     if(((Parameter & VSER_MODEM_STATUS_DSR) && (!(DriverInfoPtr->ModemStatusRegister & VSER_MODEM_STATUS_DSR))) || (!(Parameter & VSER_MODEM_STATUS_DSR) && (DriverInfoPtr->ModemStatusRegister & VSER_MODEM_STATUS_DSR)))
                     {
                        /* This is a request to either change DSR from  */
                        /* high to low or low to high.  In this case    */
                        /* increment the interrupt counter.             */
                        DriverInfoPtr->InterruptCount.dsr++;

                        ModemStatusChange = 1;
                     }

                     /* Mask the bits out of the Modem Status Register. */
                     DriverInfoPtr->ModemStatusRegister &= ~(VSER_MODEM_STATUS_CTS | VSER_MODEM_STATUS_CD | VSER_MODEM_STATUS_RNG | VSER_MODEM_STATUS_DSR);

                     /* Now set the bits that were specified.           */
                     DriverInfoPtr->ModemStatusRegister |= (unsigned int)Parameter;

                     DEBUGPRINT(KERN_ERR "%s : Modem Status 0x%08X.\n", __FUNCTION__, DriverInfoPtr->ModemStatusRegister);

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);

                     /* Check to see if a modem status change occurred  */
                     /* that should wake up any one waiting on the modem*/
                     /* status change wait queue.                       */
                     if(ModemStatusChange)
                     {
                        /* Wake up the Modem Status Wait Queue.         */
                        wake_up_interruptible(&(DriverInfoPtr->ModemStatusWaitQueue));
                     }

                     /* Set the return value to indicate success.       */
                     ret_val = 0;
                     break;
                  case VSER_IOCTL_GET_LINE_STATUS:
                     DEBUGPRINT(KERN_ERR "%s : Command VSER_IOCTL_GET_LINE_STATUS.\n", __FUNCTION__);

                     /* Simply copy the current line status into the    */
                     /* pointer supplied through the Parameter and      */
                     /* return with success to the caller.              */
                     ret_val = put_user(DriverInfoPtr->LineStatusRegister, (unsigned int *)Parameter);

                     /* Check to make sure that the Line Status Register*/
                     /* was successfully stored into the Pointer passed */
                     /* in from user space.                             */
                     if(!ret_val)
                     {
                        /* Reset the Wait Event Mask bit for Line Status*/
                        /* Events to indicate that the Line Status      */
                        /* Register has been read.                      */
                        DriverInfoPtr->WaitEventMask &= ~VSER_LINE_STATUS_EVENT;
                     }

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);
                     break;
                  case VSER_IOCTL_SET_LINE_STATUS:
                     DEBUGPRINT(KERN_ERR "%s : Command VSER_IOCTL_SET_LINE_STATUS.\n", __FUNCTION__);

                     /* First Mask off the bit with can be set by this  */
                     /* call.                                           */
                     Parameter &= (VSER_LINE_STATUS_FRAMING_ERROR | VSER_LINE_STATUS_PARITY_ERROR | VSER_LINE_STATUS_OVERRUN_ERROR);

                     /* Initialize the Flip Push variable to FALSE to   */
                     /* indicate that a TTY Flip Push does not need to  */
                     /* be done.                                        */
                     FlipPush = 0;

                     /* Next check to see if the Framing error bit is   */
                     /* set in the parameter data being passed in.      */
                     if(Parameter & VSER_LINE_STATUS_FRAMING_ERROR)
                     {
                        /* The framing error bit is set, put a NULL     */
                        /* character in the buffer with the flags set   */
                        /* to Framing error for this character.         */
                        if(tty_insert_flip_char(DriverInfoPtr->TTYStruct, '\0', TTY_FRAME))
                        {
                           /* Character was added to the buffer Change  */
                           /* Flip Push to indicate that one needs to   */
                           /* be done.                                  */
                           FlipPush = 1;
                        }

                        /* Increment the Interrupt count for Framing    */
                        /* errors.                                      */
                        DriverInfoPtr->InterruptCount.frame++;
                     }

                     /* Next check to see if the Parity error bit is set*/
                     /* in the parameter data being passed in.          */
                     if(Parameter & VSER_LINE_STATUS_PARITY_ERROR)
                     {
                        /* The parity error bit is set, put a NULL      */
                        /* character in the buffer with the flags set   */
                        /* to Parity error for this character.          */
                        if(tty_insert_flip_char(DriverInfoPtr->TTYStruct, '\0', TTY_PARITY))
                        {
                           /* Character was added to the buffer Change  */
                           /* Flip Push to indicate that one needs to   */
                           /* be done.                                  */
                           FlipPush = 1;
                        }

                        /* Increment the Interrupt count for Parity     */
                        /* errors.                                      */
                        DriverInfoPtr->InterruptCount.parity++;
                     }

                     /* Finally check to see if the Over Run error bit  */
                     /* is set in the parameter data being passed in.   */
                     if(Parameter & VSER_LINE_STATUS_OVERRUN_ERROR)
                     {
                        /* The overrun error bit is set, put a NULL     */
                        /* character in the buffer with the flags set to*/
                        /* Parity error for this character.             */
                        if(tty_insert_flip_char(DriverInfoPtr->TTYStruct, '\0', TTY_OVERRUN))
                        {
                           /* Character was added to the buffer Change  */
                           /* Flip Push to indicate that one needs to   */
                           /* be done.                                  */
                           FlipPush = 1;
                        }

                        /* Increment the Interrupt count for Over Run   */
                        /* errors.                                      */
                        DriverInfoPtr->InterruptCount.overrun++;
                     }

                     /* Update the Line Status Register.                */
                     DriverInfoPtr->LineStatusRegister &= ~(VSER_LINE_STATUS_FRAMING_ERROR | VSER_LINE_STATUS_PARITY_ERROR | VSER_LINE_STATUS_OVERRUN_ERROR);
                     DriverInfoPtr->LineStatusRegister |= (unsigned int)Parameter;

                     DEBUGPRINT(KERN_ERR "%s : Line Status 0x%08X.\n", __FUNCTION__, DriverInfoPtr->LineStatusRegister);

                     /* Set the return value to zero to indicate        */
                     /* successful execution.                           */
                     ret_val = 0;

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);

                     /* Check to see if we need to push the file input  */
                     /* buffer.                                         */
                     if(FlipPush)
                     {
                        /* Push the file input buffer to inform the TTY */
                        /* device that new data has been put into the   */
                        /* buffer.                                      */
                        tty_schedule_flip(DriverInfoPtr->TTYStruct);
                     }
                     break;
                  case VSER_IOCTL_WAIT_SERIAL_EVENT:
                     DEBUGPRINT(KERN_ERR "%s : Command VSER_IOCTL_WAIT_SERIAL_EVENT.\n", __FUNCTION__);

                     /* Get the mask of events to wait on.              */
                     ret_val = get_user(WaitEventMask, (unsigned long *)Parameter);

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);

                     /* Check to see if the return value indicates that */
                     /* an error occurred retrieving the value from user*/
                     /* space.                                          */
                     if((!ret_val) && (WaitEventMask))
                     {
                        /* Check to see if one of the specified events  */
                        /* has occurred.                                */
                        if(!(wait_event_interruptible(DriverInfoPtr->WaitEventWaitQueue, ((DriverInfoPtr->WaitEventMask & WaitEventMask) || (DriverInfoPtr->ShutDown)))))
                        {
                           /* The event has occurred return the event   */
                           /* that has occurred to the user in the      */
                           /* argument parameter.                       */
                           ret_val = put_user((DriverInfoPtr->WaitEventMask & WaitEventMask), (unsigned long *)Parameter);
                        }
                        else
                           ret_val = -EINTR;
                     }
                     else
                        ret_val = -EINVAL;
                     break;
                  case VSER_IOCTL_RECEIVE_DATA_LENGTH:
                     DEBUGPRINT(KERN_ERR "%s : Command VSER_IOCTL_RECEIVE_DATA_LENGTH.\n", __FUNCTION__);

                     /* Simply copy the Read Buffer Length into the     */
                     /* pointer supplied with the argument and return   */
                     /* with success to the caller.                     */
                     ret_val = put_user(DriverInfoPtr->ReadLength, (unsigned int *)Parameter);

                     /* Check to make sure that the Read Length was     */
                     /* successfully stored into the Pointer passed in  */
                     /* from user space.                                */
                     if(!ret_val)
                     {
                        /* The Read Length was successfully copied to   */
                        /* user space.  Reset the Read Length to zero   */
                        /* since the user has been informed of the data */
                        /* that was most recently received.             */
                        DriverInfoPtr->ReadLength = 0;

                        /* Reset the Wait Event Mask bit for Data       */
                        /* Available Events to indicate that the Length */
                        /* of Data Available has been read.             */
                        DriverInfoPtr->WaitEventMask &= ~VSER_RECEIVE_DATA_EVENT;
                     }

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);
                     break;
                  default:
                     ret_val = -ENOTTY;

                     /* Release the semaphore used to protect the Driver*/
                     /* Information Entry.                              */
                     up(&DriverInfoPtr->DriverMutex);
                     break;
               }
            }
            else
               ret_val = -EINTR;
            break;
         default:
            ret_val = -ENOTTY;
            break;
      }
   }
   else
      ret_val = -EINVAL;

   DEBUGPRINT(KERN_ERR "%s : exit, ret_val = %d\n", __FUNCTION__, ret_val);

   return(ret_val);
}

   /* These MACRO's are used to change the name of the init_module and  */
   /* cleanup_module function.                                          */
module_init(InitializeVSERModule);
module_exit(CleanupVSERModule);

